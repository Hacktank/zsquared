// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ZSquaredCharacter.h"
#include "ZSquaredPlayerCharacter.generated.h"

UENUM(BlueprintType)
enum class EPlayerCharacterAbilityType : uint8 {
	BasicAttack UMETA(DisplayName = "BasicAttack"),
	Active1 UMETA(DisplayName = "Active1"),
	Active2 UMETA(DisplayName = "Active2"),
	Active3 UMETA(DisplayName = "Active3"),
};

/**
 *
 */
UCLASS(Blueprintable)
class ZSQUARED_API AZSquaredPlayerCharacter : public AZSquaredCharacter {
	GENERATED_BODY()

public:
	// Properties
	//////////////////////////////////////////////////////////////////////////	
	UPROPERTY(BlueprintReadWrite,EditAnywhere,Category = "Z2 Character|Player")
		float XPCurrent;

	UPROPERTY(BlueprintReadWrite,EditAnywhere,Category = "Z2 Character|Player")
		float XPNextLevel;

	// Functions
	//////////////////////////////////////////////////////////////////////////
	AZSquaredPlayerCharacter();

	UFUNCTION(BlueprintImplementableEvent,Category = "Z2 Character|Player")
		void DisplayCharacterMenu();

	UFUNCTION(BlueprintPure,BlueprintNativeEvent,Category = "Z2 Character|Player|Ability")
		UCharacterAbility* GetAbilityByType(EPlayerCharacterAbilityType AbilityType) const;

protected:
	virtual UCharacterAbility* GetAbilityByType_Implementation(EPlayerCharacterAbilityType AbilityType) const;

public:
	// EVENTS
	//////////////////////////////////////////////////////////////////////////
};
