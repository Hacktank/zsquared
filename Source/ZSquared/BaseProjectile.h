// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "BaseProjectile.generated.h"

UCLASS()
class ZSQUARED_API ABaseProjectile : public AActor {
	GENERATED_BODY()

private:
	// Properties
	//////////////////////////////////////////////////////////////////////////
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category = Projectile,meta = (AllowPrivateAccess = "true"))
		UStaticMeshComponent* ProjectileMesh;

	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category = Movement,meta = (AllowPrivateAccess = "true"))
		UProjectileMovementComponent* ProjectileMovement;

public:
	UPROPERTY(BlueprintReadOnly,Category = Projectile)
		TWeakObjectPtr<UObject> ProjectileOwner;

	// Functions
	//////////////////////////////////////////////////////////////////////////
private:
	ABaseProjectile();

public:
	ABaseProjectile(UObject* ProjectileOwner);

	FORCEINLINE UStaticMeshComponent* GetProjectileMesh() const { return ProjectileMesh; }
	FORCEINLINE UProjectileMovementComponent* GetProjectileMovement() const { return ProjectileMovement; }
};
