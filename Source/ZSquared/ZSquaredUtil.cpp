#include "ZSquared.h"
#include "GameFramework/Actor.h"
#include "Runtime/AIModule/Classes/AIController.h"
#include "ZSquaredUtil.h"

UObject* UZSquaredUtilFunctions::NewObjectFromBlueprint(UObject* WorldContextObject,TSubclassOf<UObject> UC) {
	UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject);
	UObject* tempObject = NewObject<UObject>(UC);

	return tempObject;
}

void UZSquaredUtilFunctions::GetNearestActorWithLineOfSight(AAIController* Controller,TSubclassOf<AActor> ActorBaseClass,const FVector& EyeOffset,AActor*& Actor,float& Distance) {
	if(Controller == nullptr || !Controller->IsValidLowLevel() && Controller->GetPawn() != nullptr && Controller->GetPawn()->IsValidLowLevel()) return;

	TArray<AActor*> actors;
	UGameplayStatics::GetAllActorsOfClass(GEngine->GetWorldFromContextObject(Controller),ActorBaseClass,actors);

	FVector eyePoint = Controller->GetPawn()->GetActorLocation() + EyeOffset;

	Actor = nullptr;
	Distance = FLT_MAX;
	for(int32 i = 0; i < actors.Num(); i++) {
		AActor* curActor = actors[i];
		if(curActor != nullptr && curActor->IsValidLowLevel()) {
			const float curDistance = (eyePoint - curActor->GetActorLocation()).Size();
			if(curDistance < Distance && Controller->LineOfSightTo(curActor,eyePoint)) {
				Distance = curDistance;
				Actor = actors[i];
			}
		}
	}
}
