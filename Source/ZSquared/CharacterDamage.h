// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CharacterDamage.generated.h"

USTRUCT(BlueprintType)
struct ZSQUARED_API FDamageSource {
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite,EditAnywhere,Transient)
		UObject* DamageSource;

	UPROPERTY(BlueprintReadWrite,EditAnywhere,Transient)
		float DamageAmount;
};
