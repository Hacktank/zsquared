// Fill out your copyright notice in the Description page of Project Settings.

#include "ZSquared.h"
#include "ZSquaredCharacter.h"
#include "Runtime/AIModule/Classes/AIController.h"
#include "Runtime/AIModule/Classes/Blueprint/AIBlueprintHelperLibrary.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BlackBoardComponent.h"
#include "SquadComponent.h"

// Sets default values for this component's properties
USquadComponent::USquadComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;

	SquadLeader = nullptr;
}


// Called when the game starts
void USquadComponent::BeginPlay()
{
	Super::BeginPlay();

	SquadLeader = nullptr;
}


// Called every frame
void USquadComponent::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );
	// ...
	UBlackboardComponent *blackBoard = UAIBlueprintHelperLibrary::GetBlackboard(this->GetOwner());

	if(blackBoard != nullptr) {
		AActor *target = Cast<AActor>(blackBoard->GetValueAsObject("TargetCharacter"));

		if(target != nullptr) {
			if(this->SquadLeader != nullptr)	//If this isn't the squad leader
			{
				AActor *actorSquadLeader = SquadLeader->GetOwner();
				FVector rightVector = actorSquadLeader->GetActorRightVector();
				rightVector *= 200;

				FVector positionGo = rightVector + actorSquadLeader->GetActorLocation();

				APawn* ownerAsPawn = Cast<APawn>(GetOwner());
				if(ownerAsPawn != nullptr) {
					FVector directionGo = (ownerAsPawn->GetActorLocation() - positionGo).GetSafeNormal();
					ownerAsPawn->AddMovementInput(directionGo);
				}
			}
		}
	}
}

// Called when the game starts
void USquadComponent::OnUnregister()
{
	Super::OnUnregister();
	if(SquadLeader != nullptr) {
		SquadLeader->RemoveSquadMate(this);

		/*
				SquadMates = SquadMates.FilterByPredicate([this](const TWeakObjectPtr<USquadComponent>& elem)->bool {
			if(!elem.IsValid()) return false;
			auto ownerAsZ2Character = Cast<AZSquaredCharacter>(elem->GetOwner());
			if(ownerAsZ2Character != nullptr) {
				if(!ownerAsZ2Character->IsCharacterAlive()) return false;
			}
			return true;
		});
		*/
	} else {
		// we are the leader
		RemoveSquadMate(this);
	}
}

TArray<USquadComponent*> USquadComponent::GetSquadMates() const {
	
	if(SquadLeader != nullptr) {
		return SquadLeader->GetSquadMates();
	}
	else {
		// we are the leader
		TArray<USquadComponent*> allMates;
		for(auto& mate : SquadMates) {
			if(mate.IsValid()) {
				allMates.Add(mate.Get());
			}
		}
		allMates.Add((USquadComponent*)this);
		return allMates;
	}
}

void USquadComponent::AddNewSquadMate(USquadComponent* newMate)
{
	if(newMate == this) return;

	if(SquadLeader != nullptr) {
		SquadLeader->AddNewSquadMate(newMate);
	} else {
		// we are the leader
		if(!SquadMates.Contains(newMate)) {
			if(newMate->SquadLeader.IsValid()) {
				// the new mate is already in a different squad, merge the squads
				const auto otherSquadMembers = newMate->GetSquadMates();
				for(auto& mate : otherSquadMembers) {
					if(mate != nullptr) {
						mate->SquadLeader = this;
						mate->SquadMates.Empty();
					}
				}
				SquadMates.Append(otherSquadMembers);
			} else {
				SquadMates.Add(newMate);
				newMate->SquadLeader = this;
			}
		}
	}
}

void USquadComponent::RemoveSquadMate(USquadComponent *oldMate)
{
	if(oldMate == this) return;

	if(oldMate != nullptr && ((SquadLeader == nullptr && oldMate == this) || (SquadLeader != nullptr && oldMate == SquadLeader))) {
		// we must pass leadership to another member of the squad
		USquadComponent* oldLeader = nullptr;
		USquadComponent* newLeader = nullptr;
		if(SquadLeader != nullptr) {
			oldLeader = SquadLeader.Get();
			newLeader = this;
		} else {
			oldLeader = this;
			for(auto& mate : SquadMates) {
				if(mate.IsValid()) {
					newLeader = mate.Get();
					break;
				}
			}
		}

		newLeader->SquadMates = oldLeader->SquadMates;
		newLeader->SquadMates.Remove(newLeader);
		newLeader->SquadLeader = nullptr;

		for(auto& mate : newLeader->SquadMates) {
			if(mate.IsValid()) {
				mate->SquadLeader = newLeader;
			}
		}
	}

	if(SquadLeader != nullptr) {
		SquadLeader->RemoveSquadMate(oldMate);
	} else {
		// we are the leader

		if(SquadMates.Contains(oldMate)) {
			SquadMates.Remove(oldMate);
			oldMate->SquadLeader = nullptr;
		}
	}
}
