// Fill out your copyright notice in the Description page of Project Settings.

#include "ZSquared.h"
#include "FuzzySystem.h"
#include "json.h"

UFuzzySystem::UFuzzySystem() {
	DesInitialized = false;
	InitializeDesirabilityVariable();
}

UFuzzySystem::~UFuzzySystem() {
}

bool UFuzzySystem::LoadFuzzySystemFromFile(FString fileName) {
	return LoadFuzzySystem(fileName);
}

bool UFuzzySystem::DeleteVariableByName(FString name) {
	FuzzyVariable* tempVar = FindVariableByName(name);

	if(tempVar) {
		DeleteVariableByName(name);

		return true;
	}

	return false;
}

//void UFuzzySystem::ClearAllVariableDesirabilities() {
//	for(auto varIter = VariableHash.CreateIterator(); varIter; ++varIter)
//		varIter->Value->ClearDesirables();
//}

//void UFuzzySystem::ClearVariableDesirabilitiesByName(FString varName) {
//	FindVariableByName(varName)->ClearDesirables();
//}

FVector UFuzzySystem::CheckVariable(FString varName, float crispInput, EAbilityType type) {
	FuzzyVariable *var = FindVariableByName(varName);
	
	return var->CalculateDOM(crispInput, type);
}

bool UFuzzySystem::LoadFuzzySystem(FString filePath) {
	bool success = true;
	FString jsonFileString;
	TSharedPtr<FJsonObject> jsonParsed;

	FFileHelper::LoadFileToString(jsonFileString,*filePath);
	TSharedRef<TJsonReader<TCHAR>> jsonReader = TJsonReaderFactory<TCHAR>::Create(jsonFileString);

	success = FJsonSerializer::Deserialize(jsonReader,jsonParsed);

	if(!success)
		return false;

	TArray<TSharedPtr<FJsonValue>> varArr = jsonParsed->GetArrayField("variables");

	for(int32 i = 0; i < varArr.Num(); i++) {
		FuzzyVariable* tempVar = CreateFuzzyVariable(varArr[i]->AsObject()->GetStringField("variable_name"));

		TArray<TSharedPtr<FJsonValue>> valArr = varArr[i]->AsObject()->GetArrayField("values");

		for(int32 j = 0; j < valArr.Num(); j++) {
			Consequent cons = DoubleToConsequent(valArr[j]->AsObject()->GetNumberField("desirability"));

			FuzzyValue* tempVal = CreateFuzzyValue(tempVar,valArr[j]->AsObject()->GetStringField("value_name"),cons);

			TArray<TSharedPtr<FJsonValue>> lineArr = valArr[j]->AsObject()->GetArrayField("lines");

			for(int32 k = 0; k < lineArr.Num(); k++) {
				tempVal->AddLine(CreateLine(tempVal,lineArr[k]->AsObject()->GetNumberField("x1_position"),lineArr[k]->AsObject()->GetNumberField("y1_position"),lineArr[k]->AsObject()->GetNumberField("x2_position"),lineArr[k]->AsObject()->GetNumberField("y2_position")));
			}
		}
	}

	return true;
}

Consequent UFuzzySystem::DoubleToConsequent(double num) {
	if(num >= 3.0) {
		return Consequent::None;
	} else if(num >= 2.0) {
		return Consequent::VeryDesirable;
	} else if(num >= 1.0) {
		return Consequent::Desirable;
	} else if(num >= 0.0) {
		return Consequent::Undesirable;
	}

	return Consequent::None;
}

FuzzyVariable* UFuzzySystem::CreateFuzzyVariable(FString name) {
	FuzzyVariable* temp = new FuzzyVariable(name);
	VariableHash.Add(name,temp);

	return temp;
}

FuzzyValue* UFuzzySystem::CreateFuzzyValue(FuzzyVariable* var,FString name,Consequent cons) {
	FuzzyValue* temp = new FuzzyValue(name,cons);
	var->AddFuzzyValue(temp);

	return temp;
}

FuzzyLine* UFuzzySystem::CreateLine(FuzzyValue* val,float x1,float y1,float x2,float y2) {
	FuzzyLine* temp = new FuzzyLine(x1,y1,x2,y2);
	val->AddLine(temp);

	return temp;
}

FuzzyVariable* UFuzzySystem::FindVariableByName(FString name) {
	FuzzyVariable* variable = *VariableHash.Find(name);

	if(!variable)
		return NULL;

	return variable;
}

void UFuzzySystem::DeleteVariable(FString name) {
	FuzzyVariable* temp = *VariableHash.Find(name);

	VariableHash.Remove(temp->VariableName);
}

void UFuzzySystem::InitializeDesirabilityVariable() {
	//--------------------------------------------------------------------------------------------------------------------------------------
	//Desirability Variable
	//--------------------------------------------------------------------------------------------------------------------------------------

	if(!DesInitialized) {
		FuzzyVariable* desire = CreateFuzzyVariable(TEXT("desirability"));
		{
			FuzzyValue* unDes = CreateFuzzyValue(desire,TEXT("undesirable"),Consequent::None);
			{
				CreateLine(unDes,0.0f,1.0f,35.0f,0.0f);
				//CreateLine(unDes,25.0f,1.0f,50.0f,0.0f);
			}
			FuzzyValue* Des = CreateFuzzyValue(desire,TEXT("desirable"),Consequent::None);
			{
				CreateLine(Des,25.0f,0.0f,50.0f,1.0f);
				CreateLine(Des,50.0f,1.0f,75.0f,0.0f);
			}
			FuzzyValue* veryDes = CreateFuzzyValue(desire,TEXT("verydesirable"),Consequent::None);
			{
				//CreateLine(veryDes,50.0f,0.0f,75.0f,1.0f);
				CreateLine(veryDes,65.0f,0.0f,100.0f,1.0f);
			}
		}

		DesInitialized = true;
	}
}

float UFuzzySystem::And(float a,float b) {
	if(a < b)
		return a;

	return b;
}

float UFuzzySystem::Or(float a,float b) {
	if(a > b)
		return a;

	return b;
}

FVector UFuzzySystem::CalculateOperation(const TArray<FRuleParameter> paramArr) {
	bool calculating = true;
	int iter = 0;

	float udes2 = 0.0f;
	float des2 = 0.0f;
	float vdes2 = 0.0f;

	FVector desirable;

	//FuzzyVariable *tempVar = *VariableHash.Find(paramArr[iter].VariableName);

	float udes1 = paramArr[iter].Desireables.X;//tempVar->GetUnDesireableValue();
	float des1 = paramArr[iter].Desireables.Y;//tempVar->GetDesireableValue();
	float vdes1 = paramArr[iter].Desireables.Z;//tempVar->GetVeryDesireableValue();
	EOperation op = paramArr[iter].Operation;

	iter++;

	while(calculating) {
		if(op != EOperation::Not) {
			if(op == EOperation::And) {
				udes2 = paramArr[iter].Desireables.X;
				desirable.X = And(udes1,udes2);
				des2 = paramArr[iter].Desireables.Y;
				desirable.Y = And(des1,des2);
				vdes2 = paramArr[iter].Desireables.Z;
				desirable.Z = And(vdes1,vdes2);
			} else if(op == EOperation::Or) {
				udes2 = paramArr[iter].Desireables.X;
				desirable.X = Or(udes1,udes2);
				des2 = paramArr[iter].Desireables.Y;
				desirable.Y = Or(des1,des2);
				vdes2 = paramArr[iter].Desireables.Z;
				desirable.Z = Or(vdes1,vdes2);
			}

			op = paramArr[iter].Operation;
			udes1 = desirable.X;
			des1 = desirable.Y;
			vdes1 = desirable.Z;
			
			paramArr[iter].Desireables.ZeroVector;

			//ClearVariableDesirabilitiesByName(paramArr[iter].VariableName);
			iter++;

			//if(iter <= paramArr.Num() - 1) {
				//tempVar = *VariableHash.Find(paramArr[iter].VariableName);
			//}
		}
		else {
			calculating = false;
		}
	}

	

	return desirable;
}

float UFuzzySystem::CheckRule(const TArray<FRuleParameter> paramArr) {
	FVector des = CalculateOperation(paramArr);

	return Defuzzification(des);
}

float UFuzzySystem::Defuzzification(FVector desValues) {
	float udes = 0.0f;
	float des = 0.0f;
	float vdes = 0.0f;
	float tempUdes = 0.0f;
	float tempDes = 0.0f;
	float tempVdes = 0.0f;
	int32 index = 0;

	FuzzyVariable* desVar = FindVariableByName("desirability");

	for(const auto& valueIter : desVar->ValueMap) {
		for(FuzzyLine* lineIter : valueIter.Value->LineArr) {
			if(index == 0) {
				tempUdes = 0.0f;

				if(desValues.X > 0.0f)
					tempUdes = lineIter->CalculateValueWithDOM(desValues.X);

				if(tempUdes > udes)
					udes = tempUdes;
			} else if(index == 1) {
				tempDes = 0.0f;

				if(desValues.Y > 0.0f)
					tempDes = lineIter->CalculateValueWithDOM(desValues.Y);

				if(tempDes > des)
					des = tempDes;
			} else if(index == 2) {
				tempVdes = 0.0f;

				if(desValues.Z > 0.0f)
					tempVdes = lineIter->CalculateValueWithDOM(desValues.Z);

				if(tempVdes > vdes)
					vdes = tempVdes;
			}
		}
		index++;
	}

	float total = 0.0f,p1 = 0.0f,p2 = 0.0f,p3 = 0.0f;
	index = 0;

	if(udes > 0.0f) {
		index++;
		total += udes;
	}
	if(des > 0.0f) {
		index++;

		p2 = des;
		total += p2;
	}
	if(vdes > 0.0f) {
		index++;

		p3 = (vdes + 100) / 2;
		total += p3;
	}

	if(index > 0)
		return total / index;

	return 0.0f;
}