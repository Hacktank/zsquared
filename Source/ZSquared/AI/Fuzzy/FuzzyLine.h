// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
 *
 */
class ZSQUARED_API FuzzyLine {
public:
	FVector2D mPoint1,mPoint2;
	float mSlope;

public:
	FuzzyLine(float x1,float y1,float x2,float y2);
	~FuzzyLine();

	float CalculateDOM(float input,bool infinReached);
	float CalculateValueWithDOM(float value);
};
