// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "FuzzyValue.h"
#include "Ability/CharacterAbility.h"
/**
 *
 */
class ZSQUARED_API FuzzyVariable {
public:
	FString VariableName;
	TMap<FString,FuzzyValue*> ValueMap;

public:
	FuzzyVariable(FString variableName);
	~FuzzyVariable();

	void AddFuzzyValue(FuzzyValue* val);
	FVector CalculateDOM(float num,EAbilityType type);
	FuzzyValue* FindValueByName(FString name);
	FuzzyValue* GetValueByName(FString name);
};
