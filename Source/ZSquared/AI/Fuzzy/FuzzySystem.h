// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "FuzzyVariable.h"
#include "Object.h"
#include "FuzzySystem.generated.h"

/**
 *
 */
UCLASS(Blueprintable)
class ZSQUARED_API UFuzzySystem : public UObject {
	GENERATED_BODY()

public:
	TMap<FString,FuzzyVariable*> VariableHash;
	bool DesInitialized;

public:
	UFuzzySystem();
	~UFuzzySystem();

	FuzzyVariable* CreateFuzzyVariable(FString name);
	FuzzyValue* CreateFuzzyValue(FuzzyVariable* var,FString name,Consequent cons);
	FuzzyLine* CreateLine(FuzzyValue* val,float x1,float y1,float x2,float y2);
	FuzzyVariable* FindVariableByName(FString name);
	Consequent DoubleToConsequent(double num);
	bool DeleteVariableByName(FString name);
	void DeleteVariable(FString name);
	void InitializeDesirabilityVariable();
	bool LoadFuzzySystemFromFile(FString fileName);
	bool LoadFuzzySystem(FString fileName);
	//void ClearAllVariableDesirabilities();
	//void ClearVariableDesirabilitiesByName(FString ruleName);
	FVector CheckVariable(FString varName,float crispInput,EAbilityType type);
	float And(float a,float b);
	float Or(float a, float b);
	FVector CalculateOperation(const TArray<FRuleParameter> paramArr);
	float CheckRule(const TArray<FRuleParameter> paramArr);
	float Defuzzification(FVector desValues);
};
