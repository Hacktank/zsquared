// Fill out your copyright notice in the Description page of Project Settings.

#include "ZSquared.h"
#include "FuzzyValue.h"

FuzzyValue::FuzzyValue(FString valueName,Consequent fCons) {
	ValueName = valueName;
	Cons = fCons;
}

FuzzyValue::~FuzzyValue() {
}

void FuzzyValue::AddLine(FuzzyLine *line) {
	LineArr.Add(line);
}