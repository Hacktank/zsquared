// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "FuzzyLine.h"
/**
 *
 */
UENUM()
enum class Consequent : uint8 {
	Undesirable		UMETA(DisplayName = "Undesireable"),
	Desirable		UMETA(DisplayName = "Desirable"),
	VeryDesirable	UMETA(DisplayName = "Very Desirable"),
	None			UMETA(DisplayName = "None")
};

class ZSQUARED_API FuzzyValue {
public:
	Consequent Cons;
	TArray<FuzzyLine*> LineArr;
	FString ValueName;

public:
	FuzzyValue(FString valueName,Consequent fCons);
	~FuzzyValue();

	void AddLine(FuzzyLine *line);
	//float CalculateDOM(float input);
	void SetValueName(FString name) { ValueName = name; };
};
