// Fill out your copyright notice in the Description page of Project Settings.

#include "ZSquared.h"
#include "FuzzyLine.h"

FuzzyLine::FuzzyLine(float x1,float y1,float x2,float y2) {	
	mPoint1.X = x1;
	mPoint1.Y = y1;
	mPoint2.X = x2;
	mPoint2.Y = y2;

	mSlope = (mPoint2.Y - mPoint1.Y) / (mPoint2.X - mPoint1.X);
}

FuzzyLine::~FuzzyLine() {
}

float FuzzyLine::CalculateDOM(float input, bool infinReached) {
	if(input < mPoint1.X || input > mPoint2.X && !infinReached) {
		return 0.0f;
	} else if(input > mPoint2.X && infinReached)
		return mPoint2.Y;

	return (mSlope * input) - (mSlope * mPoint1.X) + mPoint1.Y;
}

float FuzzyLine::CalculateValueWithDOM(float value) {
	if(value > 1.0f || value < 0.0f) {
		return 0.0f;
	}
	if(mSlope == 0.0f)
		return mPoint1.X;
	return ((value - mPoint1.Y) / mSlope) + mPoint1.X;
}