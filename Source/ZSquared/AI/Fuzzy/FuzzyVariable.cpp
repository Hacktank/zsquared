// Fill out your copyright notice in the Description page of Project Settings.

#include "ZSquared.h"
#include "FuzzyVariable.h"

FuzzyVariable::FuzzyVariable(FString variableName) {
	VariableName = variableName;
}

FuzzyVariable::~FuzzyVariable() {
}

void FuzzyVariable::AddFuzzyValue(FuzzyValue* val) {
	ValueMap.Add(val->ValueName,val);
}

FVector FuzzyVariable::CalculateDOM(float num,EAbilityType type) {
	float dom = 0.0f;
	float temp = 0.0f;
	FVector disireables;
	uint32 iter = 0;

	for(const auto& valueIter : ValueMap) {
		dom = 0.0f;
		disireables.ZeroVector;

		if(iter > 2)
			iter = 0;

		for(FuzzyLine* lineIter : valueIter.Value->LineArr) {
			if(iter == valueIter.Value->LineArr.Num())
				temp = lineIter->CalculateDOM(num, true);
			else
				temp = lineIter->CalculateDOM(num,false);

			if(temp > dom)
				dom = temp;
		}

		if(type == EAbilityType::Offensive) {
			if(iter == 0)
				disireables.X = dom;
			else if(iter == 1)
				disireables.Y = dom;
			else if(iter == 2)
				disireables.Z = dom;
		} 
		else if(type == EAbilityType::Defensive) {
			if(iter == 0) 
				disireables.Z = dom;
			else if(iter == 1)
				disireables.Y = dom;
			else if(iter == 2)
				disireables.X = dom;
		}
		iter++;	
	}
	return disireables;
}

	FuzzyValue* FuzzyVariable::FindValueByName(FString name) {
		FuzzyValue* value = *ValueMap.Find(name);

		if(!value)
			return NULL;

		return value;
	}