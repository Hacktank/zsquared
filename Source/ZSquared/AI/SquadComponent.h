// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "SquadComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ZSQUARED_API USquadComponent : public UActorComponent
{
	GENERATED_BODY()

private:
	UPROPERTY()
		TArray<TWeakObjectPtr<USquadComponent>> SquadMates;

	UPROPERTY()
		TWeakObjectPtr<USquadComponent> SquadLeader;

public:	
	// Sets default values for this component's properties
	USquadComponent();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	virtual void OnUnregister() override;

	UFUNCTION(BlueprintCallable, Category = "Z2 Squad")
		TArray<USquadComponent*> GetSquadMates() const;

	//Add enemy to the Squad
	UFUNCTION(BlueprintCallable, Category = "Z2 Squad")
		void AddNewSquadMate(USquadComponent* newMate);

	UFUNCTION(BlueprintCallable, Category = "Z2 Squad")
		void RemoveSquadMate(USquadComponent *oldMate);
};
