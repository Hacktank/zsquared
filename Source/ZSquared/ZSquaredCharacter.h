// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "AI/Fuzzy/FuzzySystem.h"
#include "CharacterDamage.h"
#include "ZSquaredCharacter.generated.h"


UCLASS(Blueprintable,Abstract)
class ZSQUARED_API AZSquaredCharacter : public ACharacter {
	GENERATED_BODY()

private:
	// Properties
	//////////////////////////////////////////////////////////////////////////
	UPROPERTY()
		bool bIsAlive;

public:
	UPROPERTY(BlueprintReadWrite,Category = "Z2 Character|Fuzzy Logic")
		UFuzzySystem *FuzzyCalculator;

	UPROPERTY(BlueprintReadWrite,EditAnywhere,Category = "Z2 Character|Health")
		float HealthCurrent;

	UPROPERTY(BlueprintReadWrite,EditAnywhere,Category = "Z2 Character|Health")
		float HealthMaximum;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Z2 Character|Experience")
		float ExperienceCurrent;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Z2 Character|Expereience")
		float ExperienceMaximum;

	UPROPERTY(BlueprintReadWrite,EditAnywhere,Category = "Z2 Character|Health")
		float ArmorCurrent;

	UPROPERTY(BlueprintReadWrite,EditAnywhere,Category = "Z2 Character|Health")
		float ArmorMaximum;

	// Functions
	//////////////////////////////////////////////////////////////////////////
	AZSquaredCharacter();

	// TODO: add damage and other stat related members
	UFUNCTION(BlueprintCallable,Category = "Z2 Character")
		bool LoadFuzzySytemFromFile(FString fileName);

	UFUNCTION(BlueprintPure,Category = "Z2 Character")
		bool IsCharacterAlive() const;

	UFUNCTION(BlueprintCallable,Category = "Z2 Character")
		void ModifyCharacterHealth(float HealthModifier);

	UFUNCTION(BlueprintCallable, Category = "Z2 Character")
		void ModifyCharacterExperience(float ExperienceModifier);

	UFUNCTION(BlueprintCallable,Category = "Z2 Character")
		void ModifyCharacterArmor(float ArmorModifier,bool OverflowToHealth = true);

	UFUNCTION(BlueprintCallable,Category = "Z2 Character")
		void SetCharacterAlive(bool Alive);

	UFUNCTION(BlueprintCallable,Category = "Z2 Character")
		TArray<class UCharacterAbility*> GetAllCharacterAbilities() const;

	// Apply the given damage source to this character.
	// Returns the actual amount of damage applied.
	UFUNCTION(BlueprintCallable,Category = "Z2 Character")
		float ApplyDamage(FDamageSource Damage);

	UFUNCTION(BlueprintCallable,Category = "Z2 Character")
		float CalculateAbilityConfidence(UCharacterAbility* ability,TArray<float> crispArr);

	// EVENTS
	//////////////////////////////////////////////////////////////////////////

	// NOTE: The amount of damage can be modified by this function
	UFUNCTION(BlueprintNativeEvent,BlueprintCallable,Category = "Z2 Character")
		void OnDamageDealt(FDamageSource Damage,UObject* TargetObject,FDamageSource& DamageOverride);

	// NOTE: The amount of damage can be modified by this function
	UFUNCTION(BlueprintNativeEvent,BlueprintCallable,Category = "Z2 Character")
		void OnDamageReceived(FDamageSource Damage,FDamageSource& DamageOverride);

	UFUNCTION(BlueprintNativeEvent,BlueprintCallable,Category = "Z2 Character")
		void OnCharacterDeath();

	UFUNCTION(BlueprintNativeEvent,BlueprintCallable,Category = "Z2 Character")
		void OnCharacterFoeKilled(AZSquaredCharacter* Foe);

	UFUNCTION(BlueprintNativeEvent,BlueprintCallable,Category = "Z2 Character")
		void OnPossessAI();

	void BeginPlay();

	virtual void PreInitializeComponents() override;

protected:
	virtual void OnDamageDealt_Implementation(FDamageSource Damage,UObject* TargetObject,FDamageSource& DamageOverride);
	virtual void OnDamageReceived_Implementation(FDamageSource Damage,FDamageSource& DamageOverride);
	virtual void OnCharacterDeath_Implementation();
	virtual void OnCharacterFoeKilled_Implementation(AZSquaredCharacter* Foe);

	// Actually modifies health, armor, etc. Called after OnDamageDealt()/OnDamageRecieved(), from ApplyDamage()
	virtual void RecieveDamage(FDamageSource Damage);
};
