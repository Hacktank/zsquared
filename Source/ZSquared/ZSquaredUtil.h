#pragma once

#include "ZSquaredUtil.generated.h"

UCLASS()
class ZSQUARED_API UZSquaredUtilFunctions : public UBlueprintFunctionLibrary {
	GENERATED_BODY()

public:
	//CODE FROM: https://wiki.unrealengine.com/Blueprint_Node:_Create_Object_from_Blueprint
	//Copyleft under the creative commons license
	//For details see http://creativecommons.org/licenses/by-sa/4.0/
	UFUNCTION(BlueprintPure,meta = (HidePin = "WorldContextObject",DefaultToSelf = "WorldContextObject",DisplayName = "Create Object From Blueprint",CompactNodeTitle = "Create",Keywords = "new create blueprint"),Category = Game)
		static UObject* NewObjectFromBlueprint(UObject* WorldContextObject,TSubclassOf<UObject> UC);

	UFUNCTION(BlueprintPure,Category = Actor)
		static void GetNearestActorWithLineOfSight(AAIController* Controller,TSubclassOf<AActor> ActorBaseClass,const FVector& EyeOffset,AActor*& Actor,float& Distance);
};