// Fill out your copyright notice in the Description page of Project Settings.

#include "ZSquared.h"
#include "ZSquaredPlayerCharacter.h"

AZSquaredPlayerCharacter::AZSquaredPlayerCharacter() {
	GetCapsuleComponent()->SetCollisionProfileName(TEXT("PawnPlayer"));

	XPNextLevel = 100;
	XPCurrent = 0;
}

UCharacterAbility* AZSquaredPlayerCharacter::GetAbilityByType_Implementation(EPlayerCharacterAbilityType AbilityType) const {
	return nullptr;
}