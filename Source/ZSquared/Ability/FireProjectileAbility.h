// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Ability/CharacterAbility.h"
#include "FireProjectileAbility.generated.h"

/**
 *
 */
UCLASS(Blueprintable,ClassGroup = (CharacterAbility))
class ZSQUARED_API UFireProjectileAbility : public UCharacterAbility {
	GENERATED_BODY()

public:
	// Properties
	//////////////////////////////////////////////////////////////////////////
	UPROPERTY(EditInstanceOnly,BlueprintReadWrite,Category = "Character Ability|Fire Projectile")
		TWeakObjectPtr<USceneComponent> MuzzleComponent;

	UPROPERTY(EditAnywhere,Category = "Character Ability|Fire Projectile")
		TSubclassOf<class ABaseProjectile> ProjectileClass;

	// Events
	//////////////////////////////////////////////////////////////////////////
	UFUNCTION(BlueprintNativeEvent,Category = "Character Ability|Fire Projectile")
		void OnProjectileFired(ABaseProjectile* Projectile);

protected:
	virtual void OnProjectileFired_Implementation(ABaseProjectile* Projectile);

	// Overrides
	virtual void OnAbilityUse_Implementation() override;
};
