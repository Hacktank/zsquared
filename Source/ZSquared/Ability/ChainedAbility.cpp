// Fill out your copyright notice in the Description page of Project Settings.

#include "ZSquared.h"
#include "ChainedAbility.h"

UChainedAbility::UChainedAbility() {
	CurrentChainID = 0;
}

const FChainedAbilityData* UChainedAbility::GetCurrentChainDataSafe() const {
	const int32 chainDataNum = ChainData.Num();
	if(chainDataNum > 0 && CurrentChainID >= 0 && CurrentChainID < chainDataNum) return &ChainData[CurrentChainID];
	return nullptr;
}

void UChainedAbility::OnChainAbilityUse_Implementation(int32 ChainID) {
}

bool UChainedAbility::IsOffCooldown_Implementation() const {
	float currentCooldownDuration;

	const FChainedAbilityData* const currentChainData = GetCurrentChainDataSafe();
	if(currentChainData != nullptr) {
		currentCooldownDuration = currentChainData->TimeWindowMinimum;
	} else {
		currentCooldownDuration = CooldownDuration;
	}

	UWorld *currentWorld = GetWorld();
	if(currentWorld != nullptr) {
		if(currentWorld->GetTimeSeconds() >= TimeAbilityLastUsed + currentCooldownDuration) {
			return true;
		}
	}

	return false;
}

void UChainedAbility::OnAbilityUse_Implementation() {
	const FChainedAbilityData* const currentChainData = GetCurrentChainDataSafe();

	if(currentChainData != nullptr) {
		bool isWithinTimeWindow = false;

		// if we are on the first ability in the chain, the window's maximum duration is unbounded
		if(CurrentChainID > 0) {
			UWorld *currentWorld = GetWorld();
			if(currentWorld != nullptr) {
				if(currentWorld->GetTimeSeconds() <= TimeAbilityLastUsed + currentChainData->TimeWindowMaximum) {
					isWithinTimeWindow = true;
				}
			}
		} else {
			isWithinTimeWindow = true;
		}

		// if we missed the time window we must reset the chain and try the algorithm again
		if(!isWithinTimeWindow) {
			CurrentChainID = 0;
			TryUseAbility();
		} else {
			UCharacterAbility::OnAbilityUse_Implementation();
			OnChainAbilityUse(CurrentChainID);
			CurrentChainID = (CurrentChainID + 1) % ChainData.Num();
		}
	} else {
		UCharacterAbility::OnAbilityUse_Implementation();
	}
}