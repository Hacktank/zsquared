// Fill out your copyright notice in the Description page of Project Settings.

#include "ZSquared.h"
#include "FireProjectileAbility.h"

#include "BaseProjectile.h"

void UFireProjectileAbility::OnProjectileFired_Implementation(ABaseProjectile* Projectile) {
}

void UFireProjectileAbility::OnAbilityUse_Implementation() {
	UCharacterAbility::OnAbilityUse_Implementation();
	
	if(MuzzleComponent.IsValid()) {
		if(ProjectileClass != nullptr) {
			const FRotator FireRotation = MuzzleComponent->GetComponentRotation();
			const FVector FireLocation = MuzzleComponent->GetComponentLocation();

			UWorld* const world = GetWorld();
			if(world != nullptr) {
				FActorSpawnParameters spawnParams;
				spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				auto const newProjectile = world->SpawnActor<ABaseProjectile>(ProjectileClass->GetAuthoritativeClass(),FireLocation,FireRotation,spawnParams);
				if(newProjectile != nullptr) OnProjectileFired(newProjectile);
			}
		} else {
			UE_LOG(LogTemp,Warning,TEXT("%s::OnAbilityUse_Implementation(): ProjectileClass not set!"),*GetName())
		}
	} else {
		UE_LOG(LogTemp,Warning,TEXT("%s::OnAbilityUse_Implementation(): Muzzle component not set!"),*GetName())
	}
}
