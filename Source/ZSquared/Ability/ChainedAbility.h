// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Ability/CharacterAbility.h"
#include "ChainedAbility.generated.h"

USTRUCT(BlueprintType)
struct ZSQUARED_API FChainedAbilityData {
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere,BlueprintReadWrite,meta = (MakeStructureDefaultValue = "0.25"))
		float TimeWindowMinimum;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,meta = (MakeStructureDefaultValue = "1.0"))
		float TimeWindowMaximum;

	FChainedAbilityData() :
		TimeWindowMinimum(0.25f),
		TimeWindowMaximum(1.0f) {

	}
};

/**
 *
 */
UCLASS(Blueprintable)
class ZSQUARED_API UChainedAbility : public UCharacterAbility {
	GENERATED_BODY()

public:
	// Properties
	//////////////////////////////////////////////////////////////////////////
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Character Ability|Chain")
		TArray<FChainedAbilityData> ChainData;

	UPROPERTY(BlueprintReadOnly,Category = "Character Ability|Chain")
		int32 CurrentChainID;

	// Functions
	//////////////////////////////////////////////////////////////////////////
	UChainedAbility();

protected:
	const FChainedAbilityData* GetCurrentChainDataSafe() const;

	// Events
	//////////////////////////////////////////////////////////////////////////
public:
	UFUNCTION(BlueprintNativeEvent,Category = "Character Ability|Chain")
		void OnChainAbilityUse(int32 ChainID);

protected:
	virtual void OnChainAbilityUse_Implementation(int32 ChainID);

	// Overrides
	virtual bool IsOffCooldown_Implementation() const override;
	virtual void OnAbilityUse_Implementation() override;
};
