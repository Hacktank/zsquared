// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "CharacterDamage.h"
#include "CharacterAbility.generated.h"

class AZSquaredCharacter;

UENUM(BlueprintType)
enum class EOperation : uint8 {
	Not	UMETA(DisplayName = "Not"),
	Or	UMETA(DisplayName = "Or"),
	And	UMETA(DisplayName = "And")
};

UENUM(BlueprintType)
enum class EAbilityType : uint8 {
	Offensive UMETA(DisplayName = "Offensive"),
	Defensive UMETA(DisplayName = "Defensive")
};

USTRUCT(Blueprintable)
struct ZSQUARED_API FRuleParameter {
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite,EditAnywhere)
		FString VariableName;

	UPROPERTY(BlueprintReadWrite,EditAnywhere)
		EAbilityType Type;

	UPROPERTY(BlueprintReadWrite,EditAnywhere)
		EOperation Operation;

	UPROPERTY(BlueprintReadWrite,EditAnywhere)
		FVector Desireables;
};

UCLASS(Blueprintable,Abstract,ClassGroup = (CharacterAbility))
class ZSQUARED_API UCharacterAbility : public UActorComponent {
	GENERATED_BODY()

public:
	// Properties
	//////////////////////////////////////////////////////////////////////////
	UPROPERTY(BlueprintReadWrite,EditAnywhere,Category = "Character Ability")
		float CooldownDuration;

	UPROPERTY(BlueprintReadOnly,Category = "Character Ability")
		float TimeAbilityLastUsed;

	UPROPERTY(BlueprintReadWrite,Category = "Character Ability|AI")
		TArray<FRuleParameter> FuzzyRuleParams;

	UPROPERTY(BlueprintReadWrite,Category = "Character Ability|AI")
		float AIEffectiveRangeMaxiumum;

	UPROPERTY(BlueprintReadWrite,Category = "Character Ability|AI")
		float AIEffectiveRangeMinimum;

	UPROPERTY(BlueprintReadWrite,Category = "Character Ability|AI")
		FString AbilityName;

	// Functions
	//////////////////////////////////////////////////////////////////////////
	UCharacterAbility();

	UFUNCTION(BlueprintPure,Category = "Character Ability")
		float AbilityCooldownPercent() const;

	UFUNCTION(BlueprintCallable,Category = "Character Ability")
		bool TryUseAbility();

	// VIRTUALS
	//////////////////////////////////////////////////////////////////////////
	UFUNCTION(BlueprintPure,BlueprintNativeEvent,Category = "Character Ability")
		bool IsOffCooldown() const;

	UFUNCTION(BlueprintPure,BlueprintNativeEvent,Category = "Character Ability")
		bool IsAbilityUsable() const;

	// EVENTS
	//////////////////////////////////////////////////////////////////////////
	
	UFUNCTION(BlueprintImplementableEvent,BlueprintCallable,Category = "Character Ability")
		TArray<float> GetCrispInputArray(AZSquaredCharacter* Owner) const;

	UFUNCTION(BlueprintNativeEvent,BlueprintCallable,Category = "Character Ability")
		void OnAbilityUse();

	// NOTE: The amount of damage can be modified by this function
	UFUNCTION(BlueprintNativeEvent,BlueprintCallable,Category = "Character Ability")
		void OnOwnerDamageDealt(AZSquaredCharacter* Owner,FDamageSource Damage,UObject* TargetObject,FDamageSource& DamageOverride);

	// NOTE: The amount of damage can be modified by this function
	UFUNCTION(BlueprintNativeEvent,BlueprintCallable,Category = "Character Ability")
		void OnOwnerDamageReceived(AZSquaredCharacter* Owner,FDamageSource Damage,FDamageSource& DamageOverride);

	UFUNCTION(BlueprintNativeEvent,BlueprintCallable,Category = "Character Ability")
		void OnOwnerCharacterDeath(AZSquaredCharacter* Owner);

	UFUNCTION(BlueprintNativeEvent,BlueprintCallable,Category = "Character Ability")
		void OnOwnerCharacterFoeKilled(AZSquaredCharacter* Owner,AZSquaredCharacter* Foe);

protected:
	virtual bool IsOffCooldown_Implementation() const;
	virtual bool IsAbilityUsable_Implementation() const;
	//virtual TArray<float> GetCrispInputArray_Implementation(AZSquaredCharacter* Owner) const;
	virtual void OnAbilityUse_Implementation();
	virtual void OnOwnerDamageDealt_Implementation(AZSquaredCharacter* Owner,FDamageSource Damage,UObject* TargetObject,FDamageSource& DamageOverride);
	virtual void OnOwnerDamageReceived_Implementation(AZSquaredCharacter* Owner,FDamageSource Damage,FDamageSource& DamageOverride);
	virtual void OnOwnerCharacterDeath_Implementation(AZSquaredCharacter* Owner);
	virtual void OnOwnerCharacterFoeKilled_Implementation(AZSquaredCharacter* Owner,AZSquaredCharacter* Foe);
};
