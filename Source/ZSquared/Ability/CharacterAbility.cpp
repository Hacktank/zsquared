// Fill out your copyright notice in the Description page of Project Settings.

#include "ZSquared.h"
#include "CharacterAbility.h"
#include "ZSquared/ZSquaredCharacter.h"

UCharacterAbility::UCharacterAbility() {
	TimeAbilityLastUsed = -999999;

	// Defaults
	CooldownDuration = 0.05f;

	AIEffectiveRangeMaxiumum = 5000;
	AIEffectiveRangeMinimum = 0;
}

float UCharacterAbility::AbilityCooldownPercent() const {
	if(CooldownDuration <= 0) {
		return 1.0f;
	} else {
		UWorld *currentWorld = GetWorld();
		if(currentWorld != nullptr) {
			const float timeSinceLastUse = currentWorld->GetTimeSeconds() - TimeAbilityLastUsed;
			return FMath::Clamp(timeSinceLastUse / CooldownDuration,0.0f,1.0f);
		}
	}

	return 0.0f;
}

bool UCharacterAbility::TryUseAbility() {
	if(IsAbilityUsable()) {
		OnAbilityUse();
		TimeAbilityLastUsed = GetWorld()->GetTimeSeconds();
		return true;
	}
	return false;
}

bool UCharacterAbility::IsOffCooldown_Implementation() const {
	if(true) {
		UWorld *currentWorld = GetWorld();
		if(currentWorld != nullptr) {
			if(currentWorld->GetTimeSeconds() >= TimeAbilityLastUsed + CooldownDuration) {
				return true;
			}
		}
	}

	return false;
}

bool UCharacterAbility::IsAbilityUsable_Implementation() const {
	return IsOffCooldown();
}

void UCharacterAbility::OnAbilityUse_Implementation() {
	
}

void UCharacterAbility::OnOwnerDamageDealt_Implementation(AZSquaredCharacter* Owner,FDamageSource Damage,UObject* TargetObject,FDamageSource& DamageOverride) {
	DamageOverride = Damage;
}

void UCharacterAbility::OnOwnerDamageReceived_Implementation(AZSquaredCharacter* Owner,FDamageSource Damage,FDamageSource& DamageOverride) {
	DamageOverride = Damage;
}

void UCharacterAbility::OnOwnerCharacterDeath_Implementation(AZSquaredCharacter* Owner) {

}

void UCharacterAbility::OnOwnerCharacterFoeKilled_Implementation(AZSquaredCharacter* Owner,AZSquaredCharacter* Foe) {

}

//TArray<float> UCharacterAbility::GetCrispInputArray_Implementation(AZSquaredCharacter* Owner) const {
//	TArray<float> temp;
//
//	return temp;
//}