// Fill out your copyright notice in the Description page of Project Settings.

#include "ZSquared.h"
#include "AI/SquadComponent.h"
#include "ZSquaredEnemyCharacter.h"

AZSquaredEnemyCharacter::AZSquaredEnemyCharacter() {
	GetCapsuleComponent()->SetCollisionProfileName(TEXT("PawnEnemy"));

	SquadManagerComponent = CreateDefaultSubobject<USquadComponent>(TEXT("SquadManager"));
}
