// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ZSquaredCharacter.h"
#include "ZSquaredEnemyCharacter.generated.h"

/**
 *
 */
UCLASS()
class ZSQUARED_API AZSquaredEnemyCharacter : public AZSquaredCharacter {
	GENERATED_BODY()

private:
	// Properties
	//////////////////////////////////////////////////////////////////////////
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Projectile, meta = (AllowPrivateAccess = "true"))
	class USquadComponent* SquadManagerComponent;

public:

	// Functions
	//////////////////////////////////////////////////////////////////////////
	AZSquaredEnemyCharacter();

	// EVENTS
	//////////////////////////////////////////////////////////////////////////
};