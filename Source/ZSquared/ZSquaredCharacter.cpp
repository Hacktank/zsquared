// Fill out your copyright notice in the Description page of Project Settings.

#include "ZSquared.h"
#include "Ability/CharacterAbility.h"
#include "ZSquaredCharacter.h"

AZSquaredCharacter::AZSquaredCharacter() {
	HealthMaximum = HealthCurrent = 100.0f;
	ArmorMaximum = ArmorCurrent = 0.0f;
	SetCharacterAlive(true);
}

bool AZSquaredCharacter::IsCharacterAlive() const {
	return bIsAlive;
}

void AZSquaredCharacter::ModifyCharacterHealth(float HealthModifier) {
	HealthCurrent = FMath::Clamp(HealthCurrent + HealthModifier,0.0f,HealthMaximum);
	if(HealthCurrent == 0.0f) SetCharacterAlive(false);
}

void AZSquaredCharacter::ModifyCharacterExperience(float ExperienceModifier){
	ExperienceCurrent += ExperienceModifier;
	if(ExperienceCurrent >= ExperienceMaximum)
	{
		ExperienceCurrent = ExperienceCurrent - ExperienceMaximum;
		ExperienceMaximum += 50;
	}
}

void AZSquaredCharacter::ModifyCharacterArmor(float ArmorModifier,bool OverflowToHealth /*= true*/) {
	const float overflow = -(ArmorCurrent - ArmorModifier);
	ArmorCurrent = FMath::Clamp(ArmorCurrent + ArmorModifier,0.0f,ArmorMaximum);
	if(OverflowToHealth && overflow > 0.0f) {
		ModifyCharacterHealth(overflow);
	}
}

void AZSquaredCharacter::SetCharacterAlive(bool Alive) {
	if(IsCharacterAlive() && !Alive) {
		OnCharacterDeath();
	}
	bIsAlive = Alive;
}

TArray<class UCharacterAbility*> AZSquaredCharacter::GetAllCharacterAbilities() const {
	auto AllComponents = GetComponents();
	TArray<UCharacterAbility*> AllValidAbilities;

	for(auto component : AllComponents) {
		if(component != nullptr && component->IsValidLowLevel()) {
			UCharacterAbility* asCharacterAbility = Cast<UCharacterAbility>(component);
			if(asCharacterAbility != nullptr) {
				AllValidAbilities.Add(asCharacterAbility);
			}
		}
	}

	return AllValidAbilities;
}

float AZSquaredCharacter::ApplyDamage(FDamageSource Damage) {
	AZSquaredCharacter* DamageSourceAsCharacter = Cast<AZSquaredCharacter>(Damage.DamageSource);
	if(DamageSourceAsCharacter != nullptr) {
		DamageSourceAsCharacter->OnDamageDealt(Damage,this,Damage);
	}

	OnDamageReceived(Damage,Damage);
	RecieveDamage(Damage);
	return Damage.DamageAmount;
}

void AZSquaredCharacter::RecieveDamage(FDamageSource Damage) {
	ModifyCharacterArmor(Damage.DamageAmount,true);
}

void AZSquaredCharacter::OnDamageDealt_Implementation(FDamageSource Damage,UObject* TargetObject,FDamageSource& DamageOverride) {
	const auto AllAbilities = GetAllCharacterAbilities();

	for(auto const ability : AllAbilities) {
		ability->OnOwnerDamageDealt(this,Damage,TargetObject,Damage);
	}

	DamageOverride = Damage;
}

void AZSquaredCharacter::OnDamageReceived_Implementation(FDamageSource Damage,FDamageSource& DamageOverride) {
	const auto AllAbilities = GetAllCharacterAbilities();

	for(auto const ability : AllAbilities) {
		ability->OnOwnerDamageReceived(this,Damage,Damage);
	}

	DamageOverride = Damage;
}

void AZSquaredCharacter::OnCharacterDeath_Implementation() {
	const auto AllAbilities = GetAllCharacterAbilities();

	for(auto const ability : AllAbilities) {
		ability->OnOwnerCharacterDeath(this);
	}
}

void AZSquaredCharacter::OnCharacterFoeKilled_Implementation(AZSquaredCharacter* Foe) {
	const auto AllAbilities = GetAllCharacterAbilities();

	for(auto const ability : AllAbilities) {
		ability->OnOwnerCharacterFoeKilled(this,Foe);
	}
}

void AZSquaredCharacter::OnPossessAI_Implementation() {
	
}

void AZSquaredCharacter::BeginPlay() {
	FuzzyCalculator->LoadFuzzySystemFromFile(FPaths::GameDir() + TEXT("Content/Data/FuzzySystem.json"));

	Super::BeginPlay();
}

void AZSquaredCharacter::PreInitializeComponents() {
	FuzzyCalculator = NewObject<UFuzzySystem>(this,TEXT("FuzzyCalculator"));
}

bool AZSquaredCharacter::LoadFuzzySytemFromFile(FString filePath) {
	return FuzzyCalculator->LoadFuzzySystemFromFile(filePath);
}

float AZSquaredCharacter::CalculateAbilityConfidence(UCharacterAbility* ability,TArray<float> crispArr) {
	uint32 paramIter = 0;
	
	if(ability->AbilityCooldownPercent() >= 1.0f && ability->FuzzyRuleParams.Num() > 0) {
		for(int32 i = 0; i < ability->FuzzyRuleParams.Num(); i++/*FRuleParameter ruleParam : ability->FuzzyRuleParams*/) {
			FVector desireables = FuzzyCalculator->CheckVariable(ability->FuzzyRuleParams[i].VariableName,crispArr[paramIter],ability->FuzzyRuleParams[i].Type);
			ability->FuzzyRuleParams[i].Desireables = desireables;
			paramIter++;
		}

		return FuzzyCalculator->CheckRule(ability->FuzzyRuleParams);
	}

	return 0.0f;
}