#pragma once

#include "Engine/Engine.h"
//#include "DSSUtil.generated.h"

#include <tuple>

static FORCEINLINE FName GetObjPath(const UObject* Obj) {
	if(!Obj) return NAME_None;
	if(!Obj->IsValidLowLevel()) return NAME_None;
	//~

	FStringAssetReference ThePath = FStringAssetReference(Obj);

	if(!ThePath.IsValid()) return NAME_None;

	//The Class FString Name For This Object
	FString Str = Obj->GetClass()->GetDescription();

	Str += "'";
	Str += ThePath.ToString();
	Str += "'";

	return FName(*Str);
}

template <typename ObjClass>
static FORCEINLINE ObjClass* LoadObjFromPath(const FName& Path) {
	if(Path == NAME_None) return NULL;

	return Cast<ObjClass>(StaticLoadObject(ObjClass::StaticClass(),NULL,*Path.ToString()));
}

inline int factorial(int x) {
	return (x == 1 ? x : x * factorial(x - 1));
}

// returns tuple<Centroid,Area>
inline std::tuple<FVector,float> calculateCentroid2D(const TArray<FVector> &points) {
	// code by Emile Cormier: http://stackoverflow.com/questions/2792443/finding-the-centroid-of-a-polygon
	FVector centroid(0,0,0);
	float signedArea = 0;

	for(int32 i = 0; i < points.Num(); i++) {
		const FVector &p0 = points[i];
		const FVector &p1 = points[i+1 < points.Num() ? i+1 : i+1 - points.Num()];
		float a = p0.X*p1.Y - p1.X*p0.Y;
		signedArea += a;
		centroid.X += (p0.X+p1.X) * a;
		centroid.Y += (p0.Y+p1.Y) * a;
	}

	signedArea *= 0.5f;
	centroid /= 6.0f * signedArea;

	return std::make_tuple(centroid,signedArea);
}

inline FMatrix orthoMatrixFromBoundingBox(const FVector &bbmin,const FVector &bbmax) {
	FMatrix mat;
	mat.SetIdentity();
	mat.M[0][0] = 2.0f/(bbmax.X-bbmin.X);
	mat.M[1][1] = 2.0f/(bbmax.Y-bbmin.Y);
	mat.M[2][2] = 2.0f/(bbmax.Z-bbmin.Z);
	mat.M[3][0] = -(bbmax.X+bbmin.X)/(bbmax.X-bbmin.X);
	mat.M[3][1] = -(bbmax.Y+bbmin.Y)/(bbmax.Y-bbmin.Y);
	mat.M[3][2] = -(bbmax.Z+bbmin.Z)/(bbmax.Z-bbmin.Z);
	return mat;
}

template<typename T>
inline T powUINT(const T base,const uint32 exponent) {
	return (exponent == 0) ? 1 : (base * powUINT(base,exponent-1));
}
