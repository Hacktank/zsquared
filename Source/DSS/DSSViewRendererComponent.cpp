#include "DSS.h"
#include "DSSViewRendererComponent.h"
#include "Runtime/Launch/Resources/Version.h" // for ENGINE_xxx_VERSION

void FViewRendererVertexBuffer::InitRHI() {
#if ENGINE_MAJOR_VERSION >= 4 && ENGINE_MINOR_VERSION >= 3
	FRHIResourceCreateInfo CreateInfo;
	VertexBufferRHI = RHICreateVertexBuffer(vertices.Num() * sizeof(FDynamicMeshVertex),BUF_Static,CreateInfo);
#else
	VertexBufferRHI = RHICreateVertexBuffer(vertices.Num() * sizeof(FDynamicMeshVertex),NULL,BUF_Static);
#endif
	// Copy the vertex data into the vertex buffer.
	void* VertexBufferData = RHILockVertexBuffer(VertexBufferRHI,0,vertices.Num() * sizeof(FDynamicMeshVertex),RLM_WriteOnly);
	FMemory::Memcpy(VertexBufferData,vertices.GetData(),vertices.Num() * sizeof(FDynamicMeshVertex));
	RHIUnlockVertexBuffer(VertexBufferRHI);
}

void FViewRendererIndexBuffer::InitRHI() {
#if ENGINE_MAJOR_VERSION >= 4 && ENGINE_MINOR_VERSION >= 3
	FRHIResourceCreateInfo CreateInfo;
	IndexBufferRHI = RHICreateIndexBuffer(sizeof(int32),indices.Num() * sizeof(int32),BUF_Static,CreateInfo);
#else
	IndexBufferRHI = RHICreateIndexBuffer(sizeof(int32),indices.Num() * sizeof(int32),NULL,BUF_Static);
#endif
	// Write the indices to the index buffer.
	void* Buffer = RHILockIndexBuffer(IndexBufferRHI,0,indices.Num() * sizeof(int32),RLM_WriteOnly);
	FMemory::Memcpy(Buffer,indices.GetData(),indices.Num() * sizeof(int32));
	RHIUnlockIndexBuffer(IndexBufferRHI);
}

void FViewRendererVertexFactory::Init(const FViewRendererVertexBuffer* VertexBuffer) {
	check(!IsInRenderingThread());

	ENQUEUE_UNIQUE_RENDER_COMMAND_TWOPARAMETER(
		InitGeneratedMeshVertexFactory,
		FViewRendererVertexFactory*,VertexFactory,this,
		const FViewRendererVertexBuffer*,VertexBuffer,VertexBuffer,
		{
			// Initialize the vertex factory's stream components.
			DataType NewData;
			NewData.PositionComponent = STRUCTMEMBER_VERTEXSTREAMCOMPONENT(VertexBuffer,FDynamicMeshVertex,Position,VET_Float3);
			NewData.TextureCoordinates.Add(FVertexStreamComponent(VertexBuffer,STRUCT_OFFSET(FDynamicMeshVertex,TextureCoordinate),sizeof(FDynamicMeshVertex),VET_Float2));
			NewData.TangentBasisComponents[0] = STRUCTMEMBER_VERTEXSTREAMCOMPONENT(VertexBuffer,FDynamicMeshVertex,TangentX,VET_PackedNormal);
			NewData.TangentBasisComponents[1] = STRUCTMEMBER_VERTEXSTREAMCOMPONENT(VertexBuffer,FDynamicMeshVertex,TangentZ,VET_PackedNormal);
			NewData.ColorComponent = STRUCTMEMBER_VERTEXSTREAMCOMPONENT(VertexBuffer,FDynamicMeshVertex,Color,VET_Color);
			VertexFactory->SetData(NewData);
		});
}

FViewRendererVertexFactory::FViewRendererVertexFactory() {
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

UDSSViewRendererComponent::UDSSViewRendererComponent(const FObjectInitializer& PCIP)
	: Super(PCIP) {
	PrimaryComponentTick.bCanEverTick = false;
	bRenderCustomDepth = true;
}

bool UDSSViewRendererComponent::SetGeometry(const TArray<FViewRendererTriangle> &triangles) {
	mmeshTriangles = triangles;

	// Set flag that forces the recreation of the scene proxy
	MarkRenderStateDirty();

	return true;
}

FPrimitiveSceneProxy* UDSSViewRendererComponent::CreateSceneProxy() {
	//Only create if have enough triangles
	if(mmeshTriangles.Num() > 0) {
		return new FViewRendererSceneProxy(this);
	} else {
		return nullptr;
	}
}

FBoxSphereBounds UDSSViewRendererComponent::CalcBounds(const FTransform & LocalToWorld) const {
	FVector vmin(FLT_MAX,FLT_MAX,FLT_MAX),vmax(-FLT_MAX,-FLT_MAX,-FLT_MAX);

	for(int32 triIndex = 0; triIndex < mmeshTriangles.Num(); triIndex++) {
		const FViewRendererTriangle &tri = mmeshTriangles[triIndex];

		for(int i = 0; i < 3; i++) {
			vmin.X = ((vmin.X > tri.verts[i].Position.X) ? (tri.verts[i].Position.X) : (vmin.X));
			vmin.Y = ((vmin.Y > tri.verts[i].Position.Y) ? (tri.verts[i].Position.Y) : (vmin.Y));
			vmin.Z = ((vmin.Z > tri.verts[i].Position.Z) ? (tri.verts[i].Position.Z) : (vmin.Z));

			vmax.X = ((vmax.X < tri.verts[i].Position.X) ? (tri.verts[i].Position.X) : (vmax.X));
			vmax.Y = ((vmax.Y < tri.verts[i].Position.Y) ? (tri.verts[i].Position.Y) : (vmax.Y));
			vmax.Z = ((vmax.Z < tri.verts[i].Position.Z) ? (tri.verts[i].Position.Z) : (vmax.Z));
		}
	}

	FVector vecOrigin = (vmin+vmax)/2.0f;
	FVector boxBounds = vmax - vmin;

	return FBoxSphereBounds(vecOrigin,boxBounds,boxBounds.Size()).TransformBy(LocalToWorld);
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

FViewRendererSceneProxy::FViewRendererSceneProxy(UDSSViewRendererComponent *component)
	: FPrimitiveSceneProxy(component)
#if ENGINE_MAJOR_VERSION >= 4 && ENGINE_MINOR_VERSION >= 5
	,mmaterialRelevance(component->GetMaterialRelevance(ERHIFeatureLevel::SM4)) // Feature level defined by the capabilities of DX10 Shader Model 4.
#else
	,mmaterialRelevance(component->GetMaterialRelevance())
#endif
{
	bCastDynamicShadow = false;
	bCastHiddenShadow = false;
	bCastShadowAsTwoSided = false;
	bCastStaticShadow = false;
	bWillEverBeLit = false;

	// add all triangles to the vertex and index buffers
	for(int32 triIndex = 0; triIndex < component->mmeshTriangles.Num(); triIndex++) {
		FViewRendererTriangle &tri = component->mmeshTriangles[triIndex];

		// must ensure tangents are calculated
		const FVector Edge01 = (tri.verts[1].Position - tri.verts[0].Position);
		const FVector Edge02 = (tri.verts[2].Position - tri.verts[0].Position);

		const FVector TangentX = Edge01.GetSafeNormal();
		const FVector TangentZ = (Edge02 ^ Edge01).GetSafeNormal();
		const FVector TangentY = (TangentX ^ TangentZ).GetSafeNormal();

		for(int32 i = 0; i < 3; i++) {
			FDynamicMeshVertex vertTemp = tri.verts[i];
			vertTemp.SetTangents(TangentX,TangentY,TangentZ);
			int32 vertIndex = VertexBuffer.vertices.Add(vertTemp);
			IndexBuffer.indices.Add(vertIndex);
		}
	}

	// Init vertex factory
	VertexFactory.Init(&VertexBuffer);

	// Enqueue initialization of render resource
	BeginInitResource(&VertexBuffer);
	BeginInitResource(&IndexBuffer);
	BeginInitResource(&VertexFactory);

	// Grab material
	Material = component->GetMaterial(0);
	if(Material == NULL) {
		Material = UMaterial::GetDefaultMaterial(MD_Surface);
	}
}

FViewRendererSceneProxy::~FViewRendererSceneProxy() {
	VertexBuffer.ReleaseResource();
	IndexBuffer.ReleaseResource();
	VertexFactory.ReleaseResource();
}

void FViewRendererSceneProxy::GetDynamicMeshElements(const TArray<const FSceneView*>& Views,const FSceneViewFamily& ViewFamily,uint32 VisibilityMap,FMeshElementCollector& Collector) const {
	QUICK_SCOPE_CYCLE_COUNTER(STAT_ProceduralMeshSceneProxy_GetDynamicMeshElements);

	const bool bWireframe = AllowDebugViewmodes() && ViewFamily.EngineShowFlags.Wireframe;

	auto WireframeMaterialInstance = new FColoredMaterialRenderProxy(
		GEngine->WireframeMaterial ? GEngine->WireframeMaterial->GetRenderProxy(IsSelected()) : NULL,
		FLinearColor(0,0.5f,1.f)
		);

	Collector.RegisterOneFrameMaterialProxy(WireframeMaterialInstance);

	FMaterialRenderProxy* MaterialProxy = NULL;
	if(bWireframe) {
		MaterialProxy = WireframeMaterialInstance;
	} else {
		MaterialProxy = Material->GetRenderProxy(IsSelected());
	}

	for(int32 ViewIndex = 0; ViewIndex < Views.Num(); ViewIndex++) {
		if(VisibilityMap & (1 << ViewIndex)) {
			const FSceneView* View = Views[ViewIndex];
			// Draw the mesh.
			FMeshBatch& Mesh = Collector.AllocateMesh();
			FMeshBatchElement& BatchElement = Mesh.Elements[0];
			BatchElement.IndexBuffer = &IndexBuffer;
			Mesh.bWireframe = bWireframe;
			Mesh.VertexFactory = &VertexFactory;
			Mesh.MaterialRenderProxy = MaterialProxy;
			BatchElement.PrimitiveUniformBuffer = CreatePrimitiveUniformBufferImmediate(GetLocalToWorld(),GetBounds(),GetLocalBounds(),true,UseEditorDepthTest());
			BatchElement.FirstIndex = 0;
			BatchElement.NumPrimitives = IndexBuffer.indices.Num() / 3;
			BatchElement.MinVertexIndex = 0;
			BatchElement.MaxVertexIndex = VertexBuffer.vertices.Num() - 1;
			Mesh.ReverseCulling = IsLocalToWorldDeterminantNegative();
			Mesh.Type = PT_TriangleList;
			Mesh.DepthPriorityGroup = SDPG_Foreground;
			Mesh.bCanApplyViewModeOverrides = false;
			Collector.AddMesh(ViewIndex,Mesh);
		}
	}
}

// void FViewRendererSceneProxy::DrawDynamicElements(FPrimitiveDrawInterface* PDI,const FSceneView* View) {
// 	QUICK_SCOPE_CYCLE_COUNTER(STAT_ViewRendererSceneProxy_DrawDynamicElements);
//
// 	FMaterialRenderProxy* MaterialProxy = Material->GetRenderProxy(IsSelected());
//
// 	// Draw the mesh.
// 	FMeshBatch Mesh;
// 	FMeshBatchElement& BatchElement = Mesh.Elements[0];
// 	BatchElement.IndexBuffer = &IndexBuffer;
// 	Mesh.bWireframe = false; // wireframe does not apply to this mesh
// 	Mesh.VertexFactory = &VertexFactory;
// 	Mesh.MaterialRenderProxy = MaterialProxy;
// #if ENGINE_MAJOR_VERSION >= 4 && ENGINE_MINOR_VERSION >= 5
// 	BatchElement.PrimitiveUniformBuffer = CreatePrimitiveUniformBufferImmediate(GetLocalToWorld(),GetBounds(),GetLocalBounds(),true,UseEditorDepthTest());
// #else
// 	BatchElement.PrimitiveUniformBuffer = CreatePrimitiveUniformBufferImmediate(GetLocalToWorld(),GetBounds(),GetLocalBounds(),true);
// #endif
// 	BatchElement.FirstIndex = 0;
// 	BatchElement.NumPrimitives = IndexBuffer.indices.Num() / 3;
// 	BatchElement.MinVertexIndex = 0;
// 	BatchElement.MaxVertexIndex = VertexBuffer.vertices.Num() - 1;
// 	Mesh.ReverseCulling = IsLocalToWorldDeterminantNegative();
// 	Mesh.Type = PT_TriangleList;
// 	Mesh.DepthPriorityGroup = SDPG_MAX; // always render on top of everything else
// 	PDI->DrawMesh(Mesh);
// }

FPrimitiveViewRelevance FViewRendererSceneProxy::GetViewRelevance(const FSceneView* View) {
	FPrimitiveViewRelevance result;

	result.bDistortionRelevance = false;
	result.bDrawRelevance = true;
	result.bDynamicRelevance = true;
	result.bEditorNoDepthTestPrimitiveRelevance = false;
	result.bEditorPrimitiveRelevance = false;
	result.bHasSimpleLights = false;
	result.bMaskedRelevance = false;
	result.bNormalTranslucencyRelevance = false;
	result.bOpaqueRelevance = true;
	result.bRenderCustomDepth = true;
	result.bRenderInMainPass = true;
	result.bSeparateTranslucencyRelevance = false;
	result.bShadowRelevance = false;
	result.bStaticRelevance = true;

	mmaterialRelevance.SetPrimitiveViewRelevance(result);
	return result;
}

bool FViewRendererSceneProxy::CanBeOccluded() const {
	return false;
}