// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "DynamicMeshBuilder.h"
#include "DSSViewRendererComponent.generated.h"

/** Vertex Buffer */
class FViewRendererVertexBuffer : public FVertexBuffer {
public:
	TArray<FDynamicMeshVertex> vertices;

	virtual void InitRHI();
};

/** Index Buffer */
class FViewRendererIndexBuffer : public FIndexBuffer {
public:
	TArray<int32> indices;

	virtual void InitRHI();
};

/** Vertex Factory */
class FViewRendererVertexFactory : public FLocalVertexFactory {
public:
	FViewRendererVertexFactory();

	void Init(const FViewRendererVertexBuffer* VertexBuffer);
};

//////////////////////////////////////////////////////////////////////////

struct FViewRendererTriangle {
	FDynamicMeshVertex verts[3];
};

/*
Component that enables the rendering of as single part of the view plane.
*/
UCLASS()
class DSS_API UDSSViewRendererComponent : public UMeshComponent {
	GENERATED_UCLASS_BODY()

		friend class FViewRendererSceneProxy;

public:
	bool SetGeometry(const TArray<FViewRendererTriangle> &triangles);

	// Begin UMeshComponent interface.
	virtual int32 GetNumMaterials() const override { return 1; }
	// End UMeshComponent interface.

	// Begin UPrimitiveComponent interface.
	virtual FPrimitiveSceneProxy* CreateSceneProxy() override;
	// End UPrimitiveComponent interface.

private:
	// Begin USceneComponent interface.
	virtual FBoxSphereBounds CalcBounds(const FTransform & LocalToWorld) const override;
	// Begin USceneComponent interface.

	TArray<FViewRendererTriangle> mmeshTriangles;
};

class FViewRendererSceneProxy : public FPrimitiveSceneProxy {
private:
	UMaterialInterface* Material;
	FViewRendererVertexBuffer VertexBuffer;
	FViewRendererIndexBuffer IndexBuffer;
	FViewRendererVertexFactory VertexFactory;

	FMaterialRelevance mmaterialRelevance;

public:
	FViewRendererSceneProxy(UDSSViewRendererComponent *component);
	virtual ~FViewRendererSceneProxy();

	virtual void GetDynamicMeshElements(const TArray<const FSceneView*>& Views,const FSceneViewFamily& ViewFamily,uint32 VisibilityMap,FMeshElementCollector& Collector) const override;
	virtual FPrimitiveViewRelevance GetViewRelevance(const FSceneView* View);
	virtual bool CanBeOccluded() const override;

	virtual uint32 GetMemoryFootprint(void) const { return(sizeof(*this) + GetAllocatedSize()); }
	uint32 GetAllocatedSize(void) const { return(FPrimitiveSceneProxy::GetAllocatedSize()); }
};
