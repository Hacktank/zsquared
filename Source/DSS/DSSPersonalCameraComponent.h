// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "DSSPersonalCameraComponent.generated.h"

class ADSSCamera;

/**
 *
 */
UCLASS(ClassGroup = (Rendering),meta = (BlueprintSpawnableComponent))
class DSS_API UDSSPersonalCameraComponent : public UActorComponent {
	GENERATED_UCLASS_BODY()

		friend class ADSSCamera;

private:
	UPROPERTY()
		bool bIsReadyForRendering;

public:
	// Fully public properties
	UPROPERTY(BlueprintReadWrite,Category = "Dynamic Split-Screen")
		bool bPersonalCameraEnabled;

	UPROPERTY(BlueprintReadWrite,Category = "Dynamic Split-Screen")
		bool bViewMergingEnabled;

	UPROPERTY(BlueprintReadWrite,Category = "Dynamic Split-Screen",meta = (DisplayName = "Field of View",UIMin = "5.0",UIMax = "170",ClampMin = "0.001",ClampMax = "360.0"))
		float DesiredCameraFOV;

	UPROPERTY(BlueprintReadWrite,Category = "Dynamic Split-Screen",meta = (UIMin = "0.0",UIMax = "100",ClampMin = "0.0",ClampMax = "100.0"))
		float MaximumOutlineWidthScreen;

	UPROPERTY(BlueprintReadWrite,Category = "Dynamic Split-Screen",meta = (UIMin = "0.1",UIMax = "10",ClampMin = "0.1",ClampMax = "10.0"))
		float OutlineColorGradientExponent;

	UPROPERTY(BlueprintReadWrite,Category = "Dynamic Split-Screen")
		FLinearColor OutlineColor;

	// Read-only properties
	UPROPERTY(BlueprintReadOnly,Category = "Dynamic Split-Screen")
		FTransform DesiredCameraTransform;

	UPROPERTY(BlueprintReadOnly,Category = "Dynamic Split-Screen")
		FVector DesiredCameraFocalPoint;

	UPROPERTY(BlueprintReadOnly,Category = "Dynamic Split-Screen")
		TWeakObjectPtr<ADSSCamera> RegisteredDSSCamera;

	UPROPERTY(BlueprintReadOnly,Category = "Dynamic Split-Screen")
		TWeakObjectPtr<USceneCaptureComponent2D> ActualActiveCamera;

	// Public functions
	UFUNCTION(BlueprintCallable,Category = "Dynamic Split-Screen")
		void SetDesiredCameraViewOverhead(const FVector &position,const FVector &focalPoint);

	UFUNCTION(BlueprintCallable,Category = "Dynamic Split-Screen")
		bool CanViewMerge();
};
