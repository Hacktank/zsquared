// Fill out your copyright notice in the Description page of Project Settings.

#include "DSS.h"
#include "DSSPersonalCameraComponent.h"

#include "DSSCamera.h"

UDSSPersonalCameraComponent::UDSSPersonalCameraComponent(const class FObjectInitializer& PCIP)
	: Super(PCIP) {
	bIsReadyForRendering = false;
	bPersonalCameraEnabled = true;
	bViewMergingEnabled = true;
	DesiredCameraFOV = 90;
	MaximumOutlineWidthScreen = 10.0f;
	OutlineColor = FLinearColor(0,0,0,1);
	OutlineColorGradientExponent = 3.0f;
}

void UDSSPersonalCameraComponent::SetDesiredCameraViewOverhead(const FVector &position,const FVector &focalPoint) {
	bIsReadyForRendering = true;
	DesiredCameraTransform.SetIdentity();
	DesiredCameraTransform.SetTranslation(position);
	DesiredCameraFocalPoint = focalPoint;
}

bool UDSSPersonalCameraComponent::CanViewMerge() {
	// TODO: add logic here to disable merging when not in normal overhead camera mode
	return bViewMergingEnabled;
}