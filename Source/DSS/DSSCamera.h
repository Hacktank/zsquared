// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <tuple>

#include "GameFramework/Actor.h"
#include "DSSViewRendererComponent.h"
#include "DSSCamera.generated.h"

class UDSSPersonalCameraComponent;

USTRUCT()
struct DSS_API FDSSViewRendererData {
	GENERATED_USTRUCT_BODY()

		UPROPERTY(Transient)
		UDSSViewRendererComponent *RenderComponent;

	UPROPERTY(Transient)
		UTextureRenderTarget2D *CaptureTarget;

	UPROPERTY(Transient)
		USceneCaptureComponent2D *CaptureComponent;

	UPROPERTY(Transient)
		UMaterialInstanceDynamic *DynamicMaterial;
};

//////////////////////////////////////////////////////////////////////////
UCLASS()
class DSS_API ADSSCamera : public AActor {
	GENERATED_UCLASS_BODY()

		friend class UDSSPersonalCameraComponent;

private:
	// Data Structs
	//////////////////////////////////////////////////////////////////////////
	struct CenteredEllipse {
		float axisMajor;
		float axisMinor;

		float GetRadiusAtAngle(const float angle) const;

		float GetRadiusDirection2D(const FVector2D &direction) const;

		float GetRadiusDirection2D(const FVector &direction) const;
	};

	struct ViewLine {
		FVector PointOnLine;
		FVector Direction;

		FVector PlaneNormal;
		float PlaneD;

		int32 OtherProgenitor;
		int32 OtherProgenitorCompanionLineID;
		int32 ChildOutline;

		float OutlineWidthScreen;

		ViewLine(const FVector &pointonline,const FVector &direction,int32 otherprogenitor)
			: PointOnLine(pointonline),Direction(direction),OtherProgenitor(otherprogenitor) {
			PlaneNormal.Set(-direction.Y,direction.X,0);
			PlaneD = PlaneNormal | pointonline;
			ChildOutline = -1;
			OtherProgenitorCompanionLineID = -1;
			OutlineWidthScreen = 0.0f;
		}
	};

	struct ViewPoint {
		FVector Point;

		int32 ParentLineA;
		int32 ParentLineB;

		ViewPoint(const FVector &point,int32 parentlinea,int32 parentlineb)
			: Point(point),ParentLineA(parentlinea),ParentLineB(parentlineb) {
		}
	};

	struct ViewSectionPoint {
		FVector Point;
		FLinearColor Color;

		std::tuple<bool,int32> ParentLineA;
		std::tuple<bool,int32> ParentLineB;

		ViewSectionPoint(const FVector &point,const FLinearColor &color,std::tuple<bool,int32> parentlinea,std::tuple<bool,int32> parentlineb)
			: Point(point),Color(color),ParentLineA(parentlinea),ParentLineB(parentlineb) {
		}

		ViewSectionPoint() {
		}
	};

	struct ViewSection {
		TArray<ViewSectionPoint> Points;
		int32 FanPointID;

		ViewSectionPoint CentroidSplit;
	};

	struct PersonalCamera {
		TWeakObjectPtr<UDSSPersonalCameraComponent> PersonalCameraComponent;

		int32 MyViewGroupID;

		CenteredEllipse ViewEllipseWorld;
		float ViewRadiusMult;

		TArray<ViewLine> ViewLines;
		TArray<ViewPoint> ViewPointsInterior;

		TArray<ViewLine> OutlineLines;

		TArray<ViewSection> ViewSections;

		TArray<float> MergeWeight;

		FVector CentroidSplit;
		float AreaSplit;

		bool CanMergeWith(const PersonalCamera &other) const;
	};

	struct ViewGroup {
		TArray<int32> PersonalCameraIDs;

		FVector CaptureLocationWorld;
		FVector CaptureLookAtTargetWorld;
		float CaptureFOV;

		TArray<float> MergeWeight;

		FVector SplitBBMin,ScreenBBMin;
		FVector SplitBBMax,ScreenBBMax;
		FVector SplitCentroid;

		bool CanMergeWith(const TArray<PersonalCamera> &PersonalCameras,const ViewGroup &other) const;
		bool CanMergeWith(const TArray<PersonalCamera> &PersonalCameras,const PersonalCamera &other) const;
	};

	UMaterial* BaseMaterial;

	// Variables
	//////////////////////////////////////////////////////////////////////////
	UPROPERTY()
		UCameraComponent *CameraComponent;

	UPROPERTY()
		TArray<FDSSViewRendererData> RenderData;

	TArray<PersonalCamera> PersonalCameras;
	TArray<ViewGroup> ViewGroups;
	TArray<ViewLine> ViewLines;

	// Functions
	//////////////////////////////////////////////////////////////////////////
	bool BuildPersonalCameras();
	bool BuildWorldToSplitTransform();
	bool BuildSplitToRenderTransform();
	bool BuildSplitToScreenTransform();
	bool BuildPersonalCameraViewlines();
	bool BuildPersonalCameraViewPoints();
	bool BuildViewGroups();
	bool BuildPersonalCameraOutlineLines();
	bool BuildPersonalCameraOutlinePoints();
	bool BuildRenderers();

	void InitializeViewRenderData(FDSSViewRendererData *data);
	void DeinitializeViewRenderData(FDSSViewRendererData *data);

public:
	UPROPERTY(BlueprintReadOnly,Category = "Dynamic Split-Screen")
		FMatrix TransWorldToSplit;

	UPROPERTY(BlueprintReadOnly,Category = "Dynamic Split-Screen")
		FMatrix TransSplitToRender;

	UPROPERTY(BlueprintReadOnly,Category = "Dynamic Split-Screen")
		FMatrix TransSplitToScreen;

	UPROPERTY(BlueprintReadOnly,Category = "Dynamic Split-Screen")
		FMatrix TransSplitToWorldNoScale;

	UPROPERTY(BlueprintReadWrite,EditAnywhere,Category = "Dynamic Split-Screen")
		float RenderPlaneWidth;

	UPROPERTY(BlueprintReadWrite,EditAnywhere,Category = "Dynamic Split-Screen")
		float RenderPlaneDistanceFromCamera;

	UPROPERTY(BlueprintReadWrite,EditAnywhere,Category = "Dynamic Split-Screen")
		float MergeBlendDistanceWorld;

	UPROPERTY(BlueprintReadWrite,EditAnywhere,Category = "Dynamic Split-Screen")
		float MergeSkinWidth;

	virtual void Tick(float DeltaSeconds) override;
};
