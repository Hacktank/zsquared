// Fill out your copyright notice in the Description page of Project Settings.

#include "DSS.h"
#include "DSSCamera.h"

#include "DSSPersonalCameraComponent.h"

#include "DSSUtil.h"
#include <algorithm>
#include "Math.h"

float ADSSCamera::CenteredEllipse::GetRadiusDirection2D(const FVector &direction) const {
	return GetRadiusDirection2D(FVector2D(direction.X,direction.Y));
}

float ADSSCamera::CenteredEllipse::GetRadiusDirection2D(const FVector2D &direction) const {
	FVector2D directionNormalized = direction.GetSafeNormal();
	return GetRadiusAtAngle(FMath::Atan2(directionNormalized.Y,directionNormalized.X));
}

float ADSSCamera::CenteredEllipse::GetRadiusAtAngle(const float angle) const {
	return FMath::Sqrt(powUINT(axisMajor,2) * powUINT(FMath::Cos(angle),2) + powUINT(axisMinor,2) * powUINT(FMath::Sin(angle),2));
}

bool ADSSCamera::PersonalCamera::CanMergeWith(const PersonalCamera &other) const {
	// TODO: add more complex logic here to disable merging of incompatible views
	return PersonalCameraComponent->CanViewMerge() && other.PersonalCameraComponent->CanViewMerge();
}

bool ADSSCamera::ViewGroup::CanMergeWith(const TArray<PersonalCamera> &PersonalCameras,const ViewGroup &other) const {
	for(int32 i = 0; i < PersonalCameraIDs.Num(); i++) {
		for(int32 j = 0; j < other.PersonalCameraIDs.Num(); j++) {
			if(!PersonalCameras[i].CanMergeWith(PersonalCameras[other.PersonalCameraIDs[j]])) return false;
		}
	}
	return true;
}

bool ADSSCamera::ViewGroup::CanMergeWith(const TArray<PersonalCamera> &PersonalCameras,const PersonalCamera &other) const {
	for(int32 i = 0; i < PersonalCameraIDs.Num(); i++) {
		if(!PersonalCameras[i].CanMergeWith(other)) return false;
	}
	return true;
}

ADSSCamera::ADSSCamera(const class FObjectInitializer& PCIP)
	: Super(PCIP) {
	static ConstructorHelpers::FObjectFinder<UMaterial> ObjectFinderBaseMaterial(TEXT("Material'/Game/DSS/M_DSSViewRenderer.M_DSSViewRenderer'"));
	check(ObjectFinderBaseMaterial.Succeeded());
	BaseMaterial = ObjectFinderBaseMaterial.Object;

	RenderPlaneWidth = 10.0f;
	RenderPlaneDistanceFromCamera = 10.0f;

	MergeBlendDistanceWorld = 500.0f;

	MergeSkinWidth = 20.0f;

	// create and initialize the camera component
	CameraComponent = PCIP.CreateDefaultSubobject<UCameraComponent>(this,TEXT("Camera"));

	// disable post process effects on this camera, as they have been differed to the dynamic SceenCapture2D's
	CameraComponent->PostProcessBlendWeight = 0;
	CameraComponent->PostProcessSettings.AutoExposureBias = 0;

	CameraComponent->ProjectionMode = ECameraProjectionMode::Orthographic;
	CameraComponent->OrthoWidth = RenderPlaneWidth;

	RootComponent = CameraComponent;

	// tick as late as possible
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.TickGroup = TG_PostUpdateWork;
}

void ADSSCamera::Tick(float DeltaSeconds) {
	Super::Tick(DeltaSeconds);

	if(BuildPersonalCameras() &&
	   BuildWorldToSplitTransform() &&
	   BuildSplitToRenderTransform() &&
	   BuildSplitToScreenTransform() &&
	   BuildPersonalCameraViewlines() &&
	   BuildPersonalCameraViewPoints() &&
	   BuildViewGroups() &&
	   BuildPersonalCameraOutlineLines() &&
	   BuildPersonalCameraOutlinePoints() &&
	   BuildRenderers()) {
		// success
	} else {
		// failure
	}
}

bool ADSSCamera::BuildPersonalCameras() {
	if(!GEngine ||
	   !GEngine->GameViewport ||
	   !GEngine->GameViewport->Viewport) return false;

	TArray<UObject*> PersonalCameraComponents;
	GetObjectsOfClass(UDSSPersonalCameraComponent::StaticClass(),PersonalCameraComponents);

	// cull disabled and editor components
	PersonalCameraComponents = PersonalCameraComponents.FilterByPredicate([](const UObject *item)->bool {
		UDSSPersonalCameraComponent *asPersonalCamera = ((UDSSPersonalCameraComponent*)item);
		return asPersonalCamera->bPersonalCameraEnabled && asPersonalCamera->bIsReadyForRendering && asPersonalCamera->IsRegistered();
	});

	PersonalCameras.Empty(PersonalCameraComponents.Num());

	for(int32 i = 0; i < PersonalCameraComponents.Num(); i++) {
		UDSSPersonalCameraComponent *personalCameraComponent = (UDSSPersonalCameraComponent*)PersonalCameraComponents[i];

		PersonalCameras.Emplace();
		PersonalCamera &newCamera = PersonalCameras.Top();

		newCamera.PersonalCameraComponent = personalCameraComponent;
		newCamera.ViewLines.Empty(4 + PersonalCameraComponents.Num()-1);

		// expected number of view line intersections, assumes no camera-to-camera view lines are parallel
		// 1/2 (8+x(7+x)) == f(x)=3+x+f(x-1),f(0)=4
		newCamera.ViewPointsInterior.Empty(round(0.5f*(8+PersonalCameraComponents.Num()*(7+PersonalCameraComponents.Num()))));

		newCamera.MergeWeight.SetNumZeroed(PersonalCameraComponents.Num());

		; {
			FVector PersonalCameraPositionToFocalPoint = personalCameraComponent->DesiredCameraFocalPoint - personalCameraComponent->DesiredCameraTransform.GetLocation();
			float maxRadius = 2.0f * tan(FMath::DegreesToRadians(personalCameraComponent->DesiredCameraFOV) / 2.0f) * PersonalCameraPositionToFocalPoint.Size();

			// the FOV is NOT horizontal as the documentation claims, it is vertical if the aspect ratio is greater than 1, and horizontal if less
			// thus, the major and minor axes of the ellipse (x, and y) must be calculated accordingly
			FVector2D screenSize = GEngine->GameViewport->Viewport->GetSizeXY();
			if(screenSize.X > screenSize.Y) {
				newCamera.ViewEllipseWorld.axisMajor = maxRadius;
				newCamera.ViewEllipseWorld.axisMinor = maxRadius * (screenSize.Y / screenSize.X);
			} else {
				newCamera.ViewEllipseWorld.axisMajor = maxRadius * (screenSize.X / screenSize.Y);
				newCamera.ViewEllipseWorld.axisMinor = maxRadius;
			}
		}
	}

	return true;
}

bool ADSSCamera::BuildWorldToSplitTransform() {
	// get average direction and position of all personal cameras with merging enabled
	FVector averageViewPosition(0,0,0);
	FVector averageViewDirection(0,0,0);
	; {
		uint32 numPersonalCamerasIncluded = 0;
		for(int32 i = 0; i < PersonalCameras.Num(); i++) {
			if(PersonalCameras[i].PersonalCameraComponent->CanViewMerge()) {
				numPersonalCamerasIncluded++;

				const FVector &position = PersonalCameras[i].PersonalCameraComponent->DesiredCameraTransform.GetTranslation();

				averageViewPosition += position;

				const FVector &focalPoint = PersonalCameras[i].PersonalCameraComponent->DesiredCameraFocalPoint;

				FVector PersonalCameraViewDirection = focalPoint - position;
				PersonalCameraViewDirection.Normalize();
				averageViewDirection += PersonalCameraViewDirection;
			}
		}
		averageViewPosition /= (float)numPersonalCamerasIncluded;
		averageViewDirection.Normalize();
	}

	// TODO: add a check and solution for when the viewDirection is parallel with the up vector
	FMatrix viewTransform;
	viewTransform = FLookAtMatrix(averageViewPosition,averageViewPosition+averageViewDirection,FVector::UpVector);

	for(int32 i = 0; i < PersonalCameras.Num(); i++) {
		PersonalCamera &camA = PersonalCameras[i];

		FVector posSplitA = TransWorldToSplit.TransformPosition(camA.PersonalCameraComponent->DesiredCameraFocalPoint);
		posSplitA.Z = 0; // we don't care about the z component
		for(int32 j = i+1; j < PersonalCameras.Num(); j++) {
			PersonalCamera &camB = PersonalCameras[j];
			FVector posSplitB = TransWorldToSplit.TransformPosition(camB.PersonalCameraComponent->DesiredCameraFocalPoint);
		}
	}

	// calculate the view-space bounding box around all characters
	FVector viewBBMinExtent,viewBBMaxExtent;
	// 	float minimumViewSizePadding = 0;
	// 	float averageViewSizePadding = 1;
	viewBBMinExtent.Set(FLT_MAX,FLT_MAX,FLT_MAX);
	viewBBMaxExtent.Set(-FLT_MAX,-FLT_MAX,-FLT_MAX);

	for(int32 i = 0; i < PersonalCameras.Num(); i++) {
		//minimumViewSizePadding = FMath::Max(minimumViewSizePadding,PersonalCameras[i].MaximumViewRadiusWorld);

		const FVector &focalPointWorldA = PersonalCameras[i].PersonalCameraComponent->DesiredCameraFocalPoint;
		const FVector focalPointViewA = viewTransform.TransformPosition(focalPointWorldA);

		const FVector maximumPersonalViewBBExtent(PersonalCameras[i].ViewEllipseWorld.axisMajor,PersonalCameras[i].ViewEllipseWorld.axisMinor,0.0f);
		viewBBMinExtent = viewBBMinExtent.ComponentMin(focalPointViewA - maximumPersonalViewBBExtent);
		viewBBMaxExtent = viewBBMaxExtent.ComponentMax(focalPointViewA + maximumPersonalViewBBExtent);

		// 		for(int32 j = i+1; j < PersonalCameras.Num(); j++) {
		// 			const FVector &focalPointWorldB = PersonalCameras[j].PersonalCameraComponent->DesiredCameraFocalPoint;
		//
		// 			minimumViewSizePadding = FMath::Max(minimumViewSizePadding,(focalPointWorldB-focalPointWorldA).Size());
		// 			averageViewSizePadding += (focalPointWorldB-focalPointWorldA).Size();
		// 		}
	}

	//averageViewSizePadding /= factorial(PersonalCameras.Num())/(factorial(2)*factorial(PersonalCameras.Num()-2));

	//viewBBMinExtent = viewBBMinExtent - averageViewSizePadding;
	//viewBBMaxExtent = viewBBMaxExtent + averageViewSizePadding;

	// calculate an orthographic projection matrix based upon the view-space bounding box
	FMatrix orthographicTransform = orthoMatrixFromBoundingBox(viewBBMinExtent,viewBBMaxExtent);

	TransWorldToSplit = viewTransform * orthographicTransform;
	TransSplitToWorldNoScale = viewTransform.Inverse();

	return true;
}

bool ADSSCamera::BuildSplitToRenderTransform() {
	// go from the orthographic cube to the render plane in front of the camera
	FVector2D screenSize = GEngine->GameViewport->Viewport->GetSizeXY();
	float screenAspect = screenSize.Y / screenSize.X;

	FMatrix matScale = FScaleMatrix(FVector(RenderPlaneWidth/2.0f,RenderPlaneWidth/2.0f * screenAspect,0));
	FMatrix matTranslate = FTranslationMatrix(FVector(0,0,RenderPlaneDistanceFromCamera));
	FMatrix matBasis = FBasisVectorMatrix(FVector(0,0,1),FVector(1,0,0),FVector(0,1,0),FVector::ZeroVector);

	TransSplitToRender = matScale * matTranslate * matBasis /** RootComponent->ComponentToWorld.ToMatrixNoScale()*/;

	return true;
}

bool ADSSCamera::BuildSplitToScreenTransform() {
	// go from the orthographic cube to the screen
	FVector2D screenSize = GEngine->GameViewport->Viewport->GetSizeXY();

	// translate from a (-1..1, -1..1) box to a (0..2, 0..2) box
	FMatrix matTranslate = FTranslationMatrix(FVector(1,1,0));

	// scale from a (0..2, 0..2) box to a (0..screenSize.X, 0..screenSize.Y) box
	FMatrix matScale = FScaleMatrix(FVector(screenSize.X/2.0f,screenSize.Y/2.0f,0));

	TransSplitToScreen = matTranslate * matScale;

	return true;
}

bool ADSSCamera::BuildPersonalCameraViewlines() {
	// build a line bisecting the line between every pair of personal cameras
	for(int32 i = 0; i < PersonalCameras.Num(); i++) {
		PersonalCamera &camA = PersonalCameras[i];

		FVector posSplitA = TransWorldToSplit.TransformPosition(camA.PersonalCameraComponent->DesiredCameraFocalPoint);
		posSplitA.Z = 0; // we don't care about the z component
		for(int32 j = i+1; j < PersonalCameras.Num(); j++) {
			PersonalCamera &camB = PersonalCameras[j];
			FVector posSplitB = TransWorldToSplit.TransformPosition(camB.PersonalCameraComponent->DesiredCameraFocalPoint);
			posSplitB.Z = 0; // we don't care about the z component

			const FVector ab = posSplitB - posSplitA;

			// cams are on top of each other, do not create view lines between them, as they would be undefined
			if(ab.Size() < 1e-3) continue;

			const FVector abMidpoint = posSplitA + (ab * 0.5f);
			const FVector abPerp = FVector(-ab.Y,ab.X,0).GetSafeNormal2D();

			const int32 newLineIDA = camA.ViewLines.Emplace(abMidpoint,abPerp,j);
			const int32 newLineIDB = camB.ViewLines.Emplace(abMidpoint,-abPerp,i);

			camA.ViewLines[newLineIDA].OtherProgenitorCompanionLineID = newLineIDB;
			camB.ViewLines[newLineIDB].OtherProgenitorCompanionLineID = newLineIDA;
		}

		// add static lines (the edges of the split plane)
		camA.ViewLines.Emplace(FVector(-1,-1,0),FVector(1,0,0),-1);
		camA.ViewLines.Emplace(FVector(-1,-1,0),FVector(0,-1,0),-1);
		camA.ViewLines.Emplace(FVector(1,1,0),FVector(-1,0,0),-1);
		camA.ViewLines.Emplace(FVector(1,1,0),FVector(0,1,0),-1);
	}

	return true;
}

bool ADSSCamera::BuildPersonalCameraViewPoints() {
	for(int32 i = 0; i < PersonalCameras.Num(); i++) {
		PersonalCamera &cam = PersonalCameras[i];
		FVector camPosSplit = TransWorldToSplit.TransformPosition(cam.PersonalCameraComponent->DesiredCameraFocalPoint);

		// find all intersections between each pair of view lines
		for(int32 linei = 0; linei < cam.ViewLines.Num(); linei++) {
			const ViewLine &lineA = cam.ViewLines[linei];
			for(int32 linej = linei+1; linej < cam.ViewLines.Num(); linej++) {
				const ViewLine &lineB = cam.ViewLines[linej];

				FVector ab = lineB.PointOnLine - lineA.PointOnLine;
				float cross = lineA.Direction.X*lineB.Direction.Y - lineA.Direction.Y*lineB.Direction.X;

				if(FMath::Abs(cross) < 1e-3) continue; // no intersection

				const FVector intersection = lineA.PointOnLine + (lineA.Direction * (ab.X*lineB.Direction.Y - ab.Y*lineB.Direction.X) / cross);

				cam.ViewPointsInterior.Emplace(intersection,linei,linej);
			}
		}

		// cull intersection points that are on the opposite side of any line to the camera
		cam.ViewPointsInterior = MoveTemp(cam.ViewPointsInterior.FilterByPredicate([&](const ViewPoint &point)->bool {
			for(int32 linei = 0; linei < cam.ViewLines.Num(); linei++) {
				const ViewLine &line = cam.ViewLines[linei];

				// was this line one of the two parents of the point?
				if((point.ParentLineA == linei) ||
				   (point.ParentLineB == linei)) continue;

				// essentially using the point-distance-from-plane formula, specialized for being in 2D
				float intDstFromPlane = (line.PlaneNormal | point.Point) - line.PlaneD;

				if(intDstFromPlane < 0) {
					return false;
				}
			}

			return true;
		}));

		FVector pointInsideView(0,0,0);
		for(const ViewPoint &point : cam.ViewPointsInterior) {
			pointInsideView += point.Point;
		}
		pointInsideView /= (float)cam.ViewPointsInterior.Num();

		// wind the remaining points counter-clockwise
		cam.ViewPointsInterior.Sort([&](const ViewPoint &a,const ViewPoint &b)->bool {
			const float coefA = atan2(a.Point.Y-pointInsideView.Y,a.Point.X-pointInsideView.X);
			const float coefB = atan2(b.Point.Y-pointInsideView.Y,b.Point.X-pointInsideView.X);
			return coefA < coefB;
		});

		// cull points that are too linear or too close to one another
		/*while(true) {
			static const float _TOLERANCE_IS_LINEAR = 0.05f;
			static const float _TOLERANCE_TOO_CLOSE = 0.02598f;

			bool erasedAPoint = false;
			for(int32 pointi = 0; pointi < cam.ViewPointsInterior.Num(); pointi++) {
			const int32 vpindA = (pointi) % cam.ViewPointsInterior.Num();
			const int32 vpindB = (pointi+1) % cam.ViewPointsInterior.Num();

			const FVector &vppA = cam.ViewPointsInterior[vpindA];
			const FVector &vppB = cam.ViewPointsInterior[vpindB];

			const FVector ab = vppB-vppA;

			if(ab.Size2D() <= _TOLERANCE_TOO_CLOSE) {
			erasedAPoint = true;

			// between point A and B, erase the one that is closer to the cam
			const FVector camToA = vppA-camPosSplit;
			const FVector camToB = vppB-camPosSplit;
			cam.ViewPointsInterior.RemoveAt(camToA.Size2D() > camToB.Size2D() ? vpindB : vpindA);
			break;
			}

			const int32 vpindC = (pointi+2) % cam.ViewPointsInterior.Num();
			const FVector &vppC = cam.ViewPointsInterior[vpindC];
			const FVector bc = vppC-vppB;

			if(1.0f - (ab.GetSafeNormal2D() | bc.GetSafeNormal2D()) <= _TOLERANCE_IS_LINEAR) {
			erasedAPoint = true;
			cam.ViewPointsInterior.RemoveAt(vpindB);
			break;
			}
			}

			if(!erasedAPoint) break;
			}*/

		// calculate the centroid for later use
		; {
			TArray<FVector> points;
			points.Reserve(cam.ViewPointsInterior.Num());
			for(const ViewPoint &point : cam.ViewPointsInterior) {
				points.Add(point.Point);
			}

			auto centroidCalculation = calculateCentroid2D(points);
			cam.CentroidSplit = std::get<0>(centroidCalculation);
			cam.AreaSplit = std::get<1>(centroidCalculation);
		}
	}

	return true;
}

bool ADSSCamera::BuildViewGroups() {
	TArray<TSet<int32>> viewGroupCams;
	TMap<int32,int32> viewGroupByCam;

	viewGroupCams.SetNum(PersonalCameras.Num());

	// build initial view groups, a unique view group for each personal camera
	for(int32 i = 0; i < PersonalCameras.Num(); i++) {
		viewGroupCams[i].Emplace(i);
		viewGroupByCam.Emplace(i,i);
	}

	// merge intersecting view groups
	for(int32 i = 0; i < PersonalCameras.Num(); i++) {
		PersonalCamera &camI = PersonalCameras[i];

		const FVector &camFocalPosWorldI = camI.PersonalCameraComponent->DesiredCameraFocalPoint;

		for(int32 j = i+1; j < PersonalCameras.Num(); j++) {
			PersonalCamera &camJ = PersonalCameras[j];

			if(!camJ.CanMergeWith(camI)) continue;

			const FVector &camFocalPosWorldJ = camJ.PersonalCameraComponent->DesiredCameraFocalPoint;

			ViewLine *lineBisectingIJ = camI.ViewLines.FindByPredicate([&](const ViewLine &item)->bool { return item.OtherProgenitor == j; });

			FVector lineBisectingIJNormalWorld(1,0,0);

			if(lineBisectingIJ != nullptr) {
				lineBisectingIJNormalWorld = TransSplitToWorldNoScale.TransformVector(lineBisectingIJ->Direction);
			}

			float viewRadiusWorldI = camI.ViewEllipseWorld.GetRadiusDirection2D(lineBisectingIJNormalWorld);
			float viewRadiusWorldJ = camJ.ViewEllipseWorld.GetRadiusDirection2D(lineBisectingIJNormalWorld);

			// calculate intersection off the larger of the two view radii, as the smaller radius increases to match the larger one as they get closer
			float viewRadiusMaxIJWorld = FMath::Max(viewRadiusWorldI,viewRadiusWorldJ);

			if(lineBisectingIJ != nullptr) {
				viewRadiusWorldI = FMath::Abs((lineBisectingIJ->PlaneNormal | camI.CentroidSplit) - lineBisectingIJ->PlaneD) * 0.5f * viewRadiusMaxIJWorld;
				viewRadiusWorldJ = FMath::Abs((lineBisectingIJ->PlaneNormal | camJ.CentroidSplit) - lineBisectingIJ->PlaneD) * 0.5f * viewRadiusMaxIJWorld;
			}

			float seperationDistanceWorld = (camFocalPosWorldJ-camFocalPosWorldI).Size() - MergeSkinWidth;

			// calculate the blend weight of the pair I and J
			; {
				float weight = 1.0f - FMath::Clamp((seperationDistanceWorld - (viewRadiusWorldI + viewRadiusWorldJ)) / MergeBlendDistanceWorld,0.0f,1.0f);
				camI.MergeWeight[j] = weight;
				camJ.MergeWeight[i] = weight;
			}

			// already in the same view group?
			if(viewGroupByCam[i] == viewGroupByCam[j]) continue;

			// TODO: implement a way of shrinking the view spheres on the view axis into ellipsoids to ensure that cams that are way above or below one another aren't merged
			// intersecting?
			if(seperationDistanceWorld < (viewRadiusWorldI + viewRadiusWorldJ)) {
				int32 vgJ = viewGroupByCam[j];
				// TSet automatically skips duplicates
				viewGroupCams[viewGroupByCam[i]].Append(viewGroupCams[vgJ]);

				// distribute the view group merge to all of the other cams that were in camJ's view group
				for(int camInJ : viewGroupCams[vgJ]) {
					viewGroupByCam[camInJ] = viewGroupByCam[i];
				}

				// essentially marks as 'null'
				viewGroupCams[vgJ].Empty();
			}
		}
	}

	// build actual view groups based upon the results of the merge pass
	ViewGroups.Empty();

	for(int32 i = 0; i < viewGroupCams.Num(); i++) {
		if(viewGroupCams[i].Num() > 0) {
			int32 newID = ViewGroups.Emplace();

			FVector avgCaptureLocationWorld = FVector::ZeroVector;
			FVector avgCaptureLookAtTargetWorld = FVector::ZeroVector;
			FVector avgCentroidSplit = FVector::ZeroVector;
			float maxCaptureFOV = 0;
			float totalViewAreaSplit = 0.0f;

			for(int32 camToAdd : viewGroupCams[i]) {
				ViewGroups[newID].PersonalCameraIDs.Emplace(camToAdd);
				PersonalCameras[camToAdd].MyViewGroupID = newID;

				avgCaptureLocationWorld += PersonalCameras[camToAdd].PersonalCameraComponent->DesiredCameraTransform.GetTranslation();
				avgCaptureLookAtTargetWorld += PersonalCameras[camToAdd].PersonalCameraComponent->DesiredCameraFocalPoint;
				maxCaptureFOV = FMath::Max(maxCaptureFOV,PersonalCameras[camToAdd].PersonalCameraComponent->DesiredCameraFOV);

				avgCentroidSplit += PersonalCameras[camToAdd].CentroidSplit * PersonalCameras[camToAdd].AreaSplit;
				totalViewAreaSplit += PersonalCameras[camToAdd].AreaSplit;
			}

			avgCaptureLocationWorld /= (float)viewGroupCams[i].Num();
			avgCaptureLookAtTargetWorld /= (float)viewGroupCams[i].Num();
			avgCentroidSplit /= totalViewAreaSplit;

			ViewGroups[newID].CaptureLocationWorld = avgCaptureLocationWorld;
			ViewGroups[newID].CaptureLookAtTargetWorld = avgCaptureLookAtTargetWorld;
			ViewGroups[newID].CaptureFOV = maxCaptureFOV;
			ViewGroups[newID].SplitCentroid = avgCentroidSplit;

			ViewGroups[newID].MergeWeight.SetNumZeroed(viewGroupCams.Num());
		}
	}

	// calculate the merge weight between each pair of view groups
	// take the maximum merge weight of all pairs of cams in each pair of view groups
	for(int32 i = 0; i < ViewGroups.Num(); i++) {
		ViewGroup &vgI = ViewGroups[i];

		for(int32 j = i+1; j < ViewGroups.Num(); j++) {
			ViewGroup &vgJ = ViewGroups[j];

			if(!vgI.CanMergeWith(PersonalCameras,vgJ)) continue;

			float weight = 0.0f;

			for(const int32 &camIDI : vgI.PersonalCameraIDs) {
				for(const int32 &camIDJ : vgJ.PersonalCameraIDs) {
					if(camIDI == camIDJ) continue;
					weight = FMath::Max(weight,PersonalCameras[camIDI].MergeWeight[camIDJ]);
				}
			}

			vgI.MergeWeight[j] = weight;
			vgJ.MergeWeight[i] = weight;

			if(weight > 0.0f) {
				// scale the FOV of the view groups based on the weight
				; {
					ViewGroup *vgLerpA,*vgLerpB;
					if(vgI.CaptureFOV > vgJ.CaptureFOV) {
						vgLerpA = &vgJ;
						vgLerpB = &vgI;
					} else {
						vgLerpA = &vgI;
						vgLerpB = &vgJ;
					}
					vgLerpA->CaptureFOV = FMath::Lerp(vgLerpA->CaptureFOV,vgLerpB->CaptureFOV,weight);
				}

				// LERP desired camera positions between other view groups based upon merge weight
				; {
					//FVector mergedCaptureLocationWorld = (vgI.CaptureLocationWorld + vgJ.CaptureLocationWorld) * 0.5f;
					//FVector mergedCaptureLookAtTargetWorld = (vgI.CaptureLookAtTargetWorld + vgJ.CaptureLookAtTargetWorld) * 0.5f;

					//vgI.CaptureLocationWorld = FMath::Lerp(vgI.CaptureLocationWorld,mergedCaptureLocationWorld,-weight);
					//vgJ.CaptureLocationWorld = FMath::Lerp(vgJ.CaptureLocationWorld,mergedCaptureLocationWorld,-weight);

					//vgJ.CaptureLookAtTargetWorld = FMath::Lerp(vgJ.CaptureLookAtTargetWorld,mergedCaptureLookAtTargetWorld,-weight);
					//vgI.CaptureLookAtTargetWorld = FMath::Lerp(vgI.CaptureLookAtTargetWorld,mergedCaptureLookAtTargetWorld,-weight);
				}
			}
		}
	}

	return true;
}

bool ADSSCamera::BuildPersonalCameraOutlineLines() {
	// build an outline line for each view line who's MaxOutlineWidthScreen is non-zero
	FVector ScreenToSplitScale = TransSplitToScreen.GetScaleVector().Reciprocal();
	ScreenToSplitScale.Z = 0.0f;

	for(int32 i = 0; i < PersonalCameras.Num(); i++) {
		PersonalCamera &cam = PersonalCameras[i];

		for(int32 vli = 0; vli < cam.ViewLines.Num(); vli++) {
			ViewLine &viewLine = cam.ViewLines[vli];

			const float maxOutlineWidthScreen = cam.PersonalCameraComponent->MaximumOutlineWidthScreen;
			if((viewLine.OtherProgenitor >= 0 && cam.MyViewGroupID != PersonalCameras[viewLine.OtherProgenitor].MyViewGroupID && cam.CanMergeWith(PersonalCameras[viewLine.OtherProgenitor])) && maxOutlineWidthScreen > 0.0f) {
				int32 newLineID = cam.OutlineLines.Add(viewLine);

				viewLine.ChildOutline = newLineID;

				ViewLine &newLine = cam.OutlineLines[newLineID];

				newLine.OutlineWidthScreen = maxOutlineWidthScreen * (1.0f - cam.MergeWeight[viewLine.OtherProgenitor]);
				FVector offset = newLine.PlaneNormal * newLine.OutlineWidthScreen * ScreenToSplitScale;

				newLine.PointOnLine += offset;
				newLine.PlaneD = newLine.PlaneNormal | newLine.PointOnLine;
			}
		}
	}

	return true;
}

bool ADSSCamera::BuildPersonalCameraOutlinePoints() {
	FVector SplitToScreenScale = TransSplitToScreen.GetScaleVector();

	for(int32 i = 0; i < PersonalCameras.Num(); i++) {
		PersonalCamera &cam = PersonalCameras[i];
		cam.ViewSections.SetNum(powUINT(2,cam.OutlineLines.Num()));

		TArray<ViewSectionPoint> AllPoints;
		; {
			// 1/2 (-1+x) x+j (1+x) == f(x)=j-1+x+f(x-1),f(0)=j
			const int32 x = cam.OutlineLines.Num();
			const int32 j = cam.ViewLines.Num();
			AllPoints.Reserve(round(0.5f * (x-1) * (x+j) * (x+1)));
		}

		// add all exterior points
		for(const ViewPoint &point : cam.ViewPointsInterior) {
			AllPoints.Emplace(point.Point,cam.PersonalCameraComponent->OutlineColor,std::make_tuple(false,point.ParentLineA),std::make_tuple(false,point.ParentLineB));
		}

		// find all intersections between each view line/outline line pair
		for(int32 vli = 0; vli < cam.ViewLines.Num(); vli++) {
			const ViewLine &lineA = cam.ViewLines[vli];

			for(int32 olj = 0; olj < cam.OutlineLines.Num(); olj++) {
				const ViewLine &lineB = cam.OutlineLines[olj];

				FVector ab = lineB.PointOnLine - lineA.PointOnLine;
				float cross = lineA.Direction.X*lineB.Direction.Y - lineA.Direction.Y*lineB.Direction.X;

				if(FMath::Abs(cross) < 1e-3) continue; // no intersection

				FVector intersection = lineA.PointOnLine + (lineA.Direction * (ab.X*lineB.Direction.Y - ab.Y*lineB.Direction.X) / cross);

				FLinearColor pointColor = cam.PersonalCameraComponent->OutlineColor;

				// check to see if there is a matching intersection between the outline line of lineA's other progenitor
				// if so, and if it is close enough to the previously found intersection, use it instead
				// ISSUE: only works if there are exactly 2 cams in the current view group
				// TODO: add support for dynamic numbers of characters
				/*if(lineA.OtherProgenitor >= 0 &&  cam.MyViewGroupID == PersonalCameras[lineA.OtherProgenitor].MyViewGroupID && ViewGroups[cam.MyViewGroupID].PersonalCameraIDs.Num() == 2) {
					const int32 camAID = i;
					const int32 camBID = lineA.OtherProgenitor;
					const int32 camCID = lineB.OtherProgenitor;
					const PersonalCamera &camA = cam;
					const PersonalCamera &camB = PersonalCameras[camBID];
					const PersonalCamera &camC = PersonalCameras[camCID];

					const int32 targetViewLineIDC = camC.ViewLines.IndexOfByPredicate([&](const ViewLine &line)->bool {
					return line.OtherProgenitor == camBID;
					});

					if(targetViewLineIDC != INDEX_NONE) {
					const ViewLine targetOutlineLineB = camB.OutlineLines[camB.ViewLines[camC.ViewLines[targetViewLineIDC].OtherProgenitorCompanionLineID].ChildOutline];

					FVector ab = lineB.PointOnLine - targetOutlineLineB.PointOnLine;
					float cross = targetOutlineLineB.Direction.X*lineB.Direction.Y - targetOutlineLineB.Direction.Y*lineB.Direction.X;

					// valid intersection?
					if(FMath::Abs(cross) >= 1e-3) {
					const FVector intersectionAC = targetOutlineLineB.PointOnLine + (targetOutlineLineB.Direction * (ab.X*lineB.Direction.Y - ab.Y*lineB.Direction.X) / cross);
					// within split-space bounds?
					if(intersectionAC.GetAbs().GetMax() < 1.0f) {
					intersection = intersectionAC;
					} else {
					intersection = intersectionAC * (1.0f / intersectionAC.GetAbsMax());
					// avoid faint visible outlines streching across the whole screen
					pointColor.A = 0.0f;
					}
					}
					}
					}*/

				AllPoints.Emplace(intersection,pointColor,std::make_tuple(false,vli),std::make_tuple(true,olj));
			}
		}

		// find all intersections between each outline line pair
		for(int32 oli = 0; oli < cam.OutlineLines.Num(); oli++) {
			const ViewLine &lineA = cam.OutlineLines[oli];
			for(int32 olj = oli+1; olj < cam.OutlineLines.Num(); olj++) {
				const ViewLine &lineB = cam.OutlineLines[olj];

				FVector ab = lineB.PointOnLine - lineA.PointOnLine;
				float cross = lineA.Direction.X*lineB.Direction.Y - lineA.Direction.Y*lineB.Direction.X;

				if(FMath::Abs(cross) < 1e-5) continue; // no intersection

				const FVector intersection = lineA.PointOnLine + (lineA.Direction * (ab.X*lineB.Direction.Y - ab.Y*lineB.Direction.X) / cross);

				AllPoints.Emplace(intersection,cam.PersonalCameraComponent->OutlineColor,std::make_tuple(true,oli),std::make_tuple(true,olj));
			}
		}

		// cull points outside of view lines
		AllPoints = MoveTemp(AllPoints.FilterByPredicate([&](const ViewSectionPoint &point)->bool {
			for(int32 linei = 0; linei < cam.ViewLines.Num(); linei++) {
				const ViewLine &line = cam.ViewLines[linei];

				// was this line one of the two parents of the point?
				if((!std::get<0>(point.ParentLineA) && std::get<1>(point.ParentLineA) == linei) ||
				   (!std::get<0>(point.ParentLineB) && std::get<1>(point.ParentLineB) == linei)) continue;

				// essentially using the point-distance-from-plane formula, specialized for being in 2D
				float intDstFromPlane = (line.PlaneNormal | point.Point) - line.PlaneD;

				if(intDstFromPlane < 0) {
					return false;
				}
			}
			return true;
		}));

		// build all outline sections
		// the index of each section's binary representation corresponds to which side of each outline line the contained points are on
		// all points must be on the inside of all view lines
		for(int32 osi = 0; osi < cam.ViewSections.Num(); osi++) {
			ViewSection &viewSection = cam.ViewSections[osi];

			// cull points on the wrong side of outline lines
			viewSection.Points = MoveTemp(AllPoints.FilterByPredicate([&](const ViewSectionPoint &point)->bool {
				for(int32 linei = 0; linei < cam.OutlineLines.Num(); linei++) {
					const ViewLine &line = cam.OutlineLines[linei];

					// was this line one of the two parents of the point?
					if((std::get<0>(point.ParentLineA) && std::get<1>(point.ParentLineA) == linei) ||
					   (std::get<0>(point.ParentLineB) && std::get<1>(point.ParentLineB) == linei)) continue;

					const bool targetSide = (osi & (0x1 << linei)) != 0;

					// essentially using the point-distance-from-plane formula, specialized for being in 2D
					float intDstFromPlane = (line.PlaneNormal | point.Point) - line.PlaneD;

					if(intDstFromPlane > 0 != targetSide) {
						return false;
					}
				}

				return true;
			}));

			auto lam_generatePointColor = [&](ViewSectionPoint &point)->void {
				int32 nearestViewLine = -1;
				float nearestViewLineDistance = FLT_MAX;

				for(int32 linei = 0; linei < cam.ViewLines.Num(); linei++) {
					const ViewLine &line = cam.ViewLines[linei];

					const bool isMergable = line.OtherProgenitor >= 0 &&  cam.MyViewGroupID != PersonalCameras[line.OtherProgenitor].MyViewGroupID && cam.CanMergeWith(PersonalCameras[line.OtherProgenitor]);

					if(isMergable) {
						float intDstFromPlane = (line.PlaneNormal | point.Point) - line.PlaneD;

						if(intDstFromPlane < nearestViewLineDistance) {
							nearestViewLine = linei;
							nearestViewLineDistance = intDstFromPlane;
						}
					}
				}

				float colorMult = 0;

				if(nearestViewLine >= 0 && cam.ViewLines[nearestViewLine].ChildOutline >= 0) {
					const float distanceScreen = (cam.ViewLines[nearestViewLine].PlaneNormal * nearestViewLineDistance * SplitToScreenScale).Size2D();
					const float maxWidth = cam.OutlineLines[cam.ViewLines[nearestViewLine].ChildOutline].OutlineWidthScreen;

					if(maxWidth > 0)
						colorMult = 1.0f - FMath::Min(1.0f,distanceScreen / (maxWidth*0.98f));
					else
						colorMult = 0;
				}

				point.Color.A *= colorMult;
			};

			// update colors based upon the distance from it to the nearest mergeable view line
			FVector pointInsideSection(0,0,0);
			for(ViewSectionPoint &vsPoint : viewSection.Points) {
				pointInsideSection += vsPoint.Point;
				lam_generatePointColor(vsPoint);
			}
			pointInsideSection /= (float)viewSection.Points.Num();

			// wind the remaining points counter-clockwise
			viewSection.Points.Sort([&](const ViewSectionPoint &a,const ViewSectionPoint &b)->bool {
				const float coefA = atan2(a.Point.Y-pointInsideSection.Y,a.Point.X-pointInsideSection.X);
				const float coefB = atan2(b.Point.Y-pointInsideSection.Y,b.Point.X-pointInsideSection.X);
				return coefA < coefB;
			});

			// calculate the point to use as the fan point
			// the point with the lowest alpha color value
			viewSection.FanPointID = 0;
			for(int32 i = 1; i < viewSection.Points.Num(); i++) {
				if(viewSection.Points[i].Color.A < viewSection.Points[viewSection.FanPointID].Color.A) {
					viewSection.FanPointID = i;
				}
			}

			// 			; {
			// 				TArray<FVector> points;
			// 				points.Reserve(viewSection.Points.Num());
			// 				for(const ViewSectionPoint &point : viewSection.Points) {
			// 					points.Add(point.Point);
			// 				}
			//
			// 				auto centroidCalculation = calculateCentroid2D(points);
			// 				viewSection.CentroidSplit.Point = std::get<0>(centroidCalculation);
			//
			// 				lam_generatePointColor(viewSection.CentroidSplit);
			// 				viewSection.FanPointID = 0;
			// 			}
		}
	}

	return true;
}

bool ADSSCamera::BuildRenderers() {
	// ensure there is exactly one render data instance per view group
	; {
		int32 oldNumRenderComponents = RenderData.Num();

		// deinitialize old, unnecessary data
		for(int32 vgi = oldNumRenderComponents-1; vgi >= ViewGroups.Num(); vgi--) {
			DeinitializeViewRenderData(&RenderData[vgi]);
		}

		RenderData.SetNum(ViewGroups.Num());

		// initialize new data
		for(int32 vgi = oldNumRenderComponents; vgi < RenderData.Num(); vgi++) {
			InitializeViewRenderData(&RenderData[vgi]);
		}
	}

	// synchronize capture component and capture target data
	for(int32 vgi = 0; vgi < ViewGroups.Num(); vgi++) {
		// build a screen-space bounding box around the view group's screen area
		ViewGroups[vgi].SplitBBMin.Set(FLT_MAX,FLT_MAX,0);
		ViewGroups[vgi].SplitBBMax.Set(-FLT_MAX,-FLT_MAX,0);
		for(int32 pci = 0; pci < ViewGroups[vgi].PersonalCameraIDs.Num(); pci++) {
			const PersonalCamera &cam = PersonalCameras[ViewGroups[vgi].PersonalCameraIDs[pci]];

			for(int32 vlii = 0; vlii < cam.ViewPointsInterior.Num(); vlii++) {
				const FVector &intersectionPointSplit = cam.ViewPointsInterior[vlii].Point;
				ViewGroups[vgi].SplitBBMin = ViewGroups[vgi].SplitBBMin.ComponentMin(intersectionPointSplit);
				ViewGroups[vgi].SplitBBMax = ViewGroups[vgi].SplitBBMax.ComponentMax(intersectionPointSplit);
			}

			RenderData[vgi].DynamicMaterial->SetScalarParameterValue("outlineFalloffExponent",cam.PersonalCameraComponent->OutlineColorGradientExponent);
		}

		ViewGroups[vgi].ScreenBBMin = TransSplitToScreen.TransformPosition(ViewGroups[vgi].SplitBBMin);
		ViewGroups[vgi].ScreenBBMax = TransSplitToScreen.TransformPosition(ViewGroups[vgi].SplitBBMax);

		// set capture target sizes based upon the screen-space bounding box
		RenderData[vgi].CaptureTarget->SizeX = ViewGroups[vgi].ScreenBBMax.X - ViewGroups[vgi].ScreenBBMin.X;
		RenderData[vgi].CaptureTarget->SizeY = ViewGroups[vgi].ScreenBBMax.Y - ViewGroups[vgi].ScreenBBMin.Y;
		RenderData[vgi].CaptureTarget->UpdateResource();

		// calculate the transform of the capture component
		; {
			// we must offset the camera eye and target to ensure that the target appears in the center of the viewing area.
			// the offset vector is calculated by taking the split-space difference between the bounding-box center, and the
			// center of the actual viewing area, and converting it back into world-space.

			// TODO: this method does not support irregular views, ones that aren't intended to merge, such as first person.
			//       come up with some way to alleviate this
			//       also: apply a multiplier to this effect based upon how close to merging two characters are, this will
			//             alleviate snapping that occurs
			const FVector SplitBBCenter = (ViewGroups[vgi].SplitBBMin + ViewGroups[vgi].SplitBBMax) * 0.5f;
			const FVector &SplitViewAreaCenter = ViewGroups[vgi].SplitCentroid;
			const float ViewAreaSize = PersonalCameras[ViewGroups[vgi].PersonalCameraIDs[0]].ViewEllipseWorld.GetRadiusDirection2D(SplitBBCenter - SplitViewAreaCenter);

			FVector WorldCameraOffset = TransSplitToWorldNoScale.TransformVector((SplitBBCenter - SplitViewAreaCenter)) * ViewAreaSize * 0.5f;

			// TODO: modify WorldCameraOffset such that if the view groups are within merge weighting distance the players lerp into their correct relative positions
			/*; {
				// current state:
				//   multiplier aggregation is correct
				//   application of the multiplier does not produce desired effect
				// possible implementation paths:
				//   convert the world vector between the two view groups into screen space and scale it such that it is the proper distance apart
				float mult = 1.0f;
				for(int32 vgj = 0; vgj < ViewGroups.Num(); vgj++) {
				if(vgi==vgj) continue;
				mult *= ViewGroups[vgi].MergeWeight[vgj];
				}
				WorldCameraOffset *= 1.0f - mult;
				}*/

			const FVector &eyePosition = ViewGroups[vgi].CaptureLocationWorld + WorldCameraOffset;
			const FVector &targetPosition = ViewGroups[vgi].CaptureLookAtTargetWorld + WorldCameraOffset;

			// must invert the Z parameter of the look direction to get past UE's bizarro coordinate system, otherwise the camera will face 180deg the wrong way on the Y axis
			FMatrix matLookatDX = FLookAtMatrix(FVector::ZeroVector,(targetPosition-eyePosition)*FVector(1,1,-1),FVector::UpVector);
			FMatrix matBasis = FBasisVectorMatrix(FVector(0,0,1),FVector(1,0,0),FVector(0,1,0),FVector::ZeroVector);
			FMatrix matTranslation = FTranslationMatrix(eyePosition);

			FMatrix matNewTransform = (matLookatDX * matBasis * matTranslation);

			RenderData[vgi].CaptureComponent->SetRelativeTransform(FTransform(matNewTransform));
		}

		// scale the FOV of the capture component based upon the desired FOV and the view area's width relative to the total view port width
		// this is to prevent visible scaling of the captured scene
		; {
			// the FOV is NOT horizontal as the documentation claims, it is vertical if the aspect ratio is greater than 1, and horizontal if less
			// thus, the multiplier is calculated off the larger dimension
			float mult =
				FMath::Max(RenderData[vgi].CaptureTarget->SizeX,RenderData[vgi].CaptureTarget->SizeY) /
				FMath::Max((float)GEngine->GameViewport->Viewport->GetSizeXY().X,(float)GEngine->GameViewport->Viewport->GetSizeXY().Y);

			float fullSize = 2.0f * tan(FMath::DegreesToRadians(ViewGroups[vgi].CaptureFOV)/2.0f);
			float actualSize = fullSize * mult;
			RenderData[vgi].CaptureComponent->FOVAngle = FMath::RadiansToDegrees(2.0f * atan(actualSize/2.0f));
		}
	}

	// set new geometry for renderers
	for(int32 vgi = 0; vgi < ViewGroups.Num(); vgi++) {
		TArray<FViewRendererTriangle> triangles;

		for(int32 pci = 0; pci < ViewGroups[vgi].PersonalCameraIDs.Num(); pci++) {
			PersonalCamera &cam = PersonalCameras[ViewGroups[vgi].PersonalCameraIDs[pci]];

			auto lam_generateVertexFromSplitPoint = [&](const FVector &pointSplit)->FDynamicMeshVertex {
				FDynamicMeshVertex ret;
				ret.Position = TransSplitToRender.TransformPosition(pointSplit);

				FVector pointScreen = TransSplitToScreen.TransformPosition(pointSplit);

				// texture coordinates are the screen point projected into the screen-space bounding box and then normalized
				ret.TextureCoordinate.X = (pointScreen.X-ViewGroups[vgi].ScreenBBMin.X) / (ViewGroups[vgi].ScreenBBMax.X - ViewGroups[vgi].ScreenBBMin.X);
				ret.TextureCoordinate.Y = 1.0f - ((pointScreen.Y-ViewGroups[vgi].ScreenBBMin.Y) / (ViewGroups[vgi].ScreenBBMax.Y - ViewGroups[vgi].ScreenBBMin.Y));

				return ret;
			};

			for(int32 vsi = 0; vsi < cam.ViewSections.Num(); vsi++) {
				const ViewSection &viewSection = cam.ViewSections[vsi];

				if(viewSection.Points.Num() < 3) continue;

				const ViewSectionPoint &fanPoint = viewSection.Points[viewSection.FanPointID];
				//const ViewSectionPoint &fanPoint = viewSection.CentroidSplit;

				const FDynamicMeshVertex vertFanPoint = lam_generateVertexFromSplitPoint(fanPoint.Point);
				for(int32 tri = viewSection.FanPointID + 1; tri < viewSection.Points.Num() + viewSection.FanPointID - 1; tri++) {
					//for(int32 tri = 0; tri < viewSection.Points.Num(); tri++) {
					FViewRendererTriangle triangle;

					const int32 bInd = (tri) % viewSection.Points.Num();
					const int32 cInd = (tri+1) % viewSection.Points.Num();

					triangle.verts[0] = vertFanPoint;
					triangle.verts[0].Color = fanPoint.Color.ToFColor(false);

					triangle.verts[1] = lam_generateVertexFromSplitPoint(viewSection.Points[bInd].Point);
					triangle.verts[1].Color = viewSection.Points[bInd].Color.ToFColor(false);

					triangle.verts[2] = lam_generateVertexFromSplitPoint(viewSection.Points[cInd].Point);
					triangle.verts[2].Color = viewSection.Points[cInd].Color.ToFColor(false);

					triangles.Add(triangle);
				}
			}
		}

		RenderData[vgi].RenderComponent->SetGeometry(triangles);
	}

	// ensure the actual camera component's values are set properly
	CameraComponent->ProjectionMode = ECameraProjectionMode::Orthographic;
	CameraComponent->OrthoWidth = RenderPlaneWidth;

	return true;
}

void ADSSCamera::InitializeViewRenderData(FDSSViewRendererData *data) {
	// TODO: create and utilize template objects

	// Capture Target
	data->CaptureTarget = NewObject<UTextureRenderTarget2D>(this);
	data->CaptureTarget->UpdateResource();
	data->CaptureTarget->bHDR = false;
	data->CaptureTarget->AddressX = TA_Clamp;
	data->CaptureTarget->AddressY = TA_Clamp;

	// Render Component
	data->RenderComponent = NewObject<UDSSViewRendererComponent>(this);
	data->RenderComponent->SetRelativeLocation(FVector::ZeroVector);
	data->RenderComponent->SetRelativeRotation(FRotator::ZeroRotator);
	data->RenderComponent->AttachTo(RootComponent);
	data->RenderComponent->RegisterComponent();
	data->RenderComponent->Activate();

	// Render Material
	data->DynamicMaterial = data->RenderComponent->CreateDynamicMaterialInstance(0,BaseMaterial);
	data->DynamicMaterial->SetTextureParameterValue("tex_diffuse",data->CaptureTarget);

	// Capture Component
	data->CaptureComponent = NewObject<USceneCaptureComponent2D>(this);
	data->CaptureComponent->bAbsoluteLocation = true;
	data->CaptureComponent->bAbsoluteRotation = true;
	data->CaptureComponent->bAbsoluteScale = true;
	data->CaptureComponent->ShowFlags.Decals = true;
	data->CaptureComponent->ShowFlags.PostProcessing = true;
	data->CaptureComponent->ShowFlags.PostProcessMaterial = true;
	data->CaptureComponent->RegisterComponent();
	data->CaptureComponent->Activate();
	data->CaptureComponent->CaptureSource = SCS_FinalColorLDR;
	data->CaptureComponent->bCaptureEveryFrame = true;
	data->CaptureComponent->TextureTarget = data->CaptureTarget;
}

void ADSSCamera::DeinitializeViewRenderData(FDSSViewRendererData *data) {
	data->DynamicMaterial->ConditionalBeginDestroy();
	data->DynamicMaterial = nullptr;

	data->RenderComponent->DestroyComponent();
	data->RenderComponent = nullptr;

	data->CaptureComponent->DestroyComponent();
	data->CaptureComponent = nullptr;

	data->CaptureTarget->ConditionalBeginDestroy();
	data->CaptureTarget = nullptr;
}