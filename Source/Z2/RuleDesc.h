// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "FuzzyVariable.h"
/**
 * 
 */
enum Operation{
	AND,
	OR,
	NOT
};

class Z2_API RuleDesc
{
private:
	float mCrispInput;
	Operation mOperation;
	FuzzyVariable *mVariablePtr;

public:
	RuleDesc(FuzzyVariable *varPtr, Operation opo);
	~RuleDesc();

	void CalculateDesirables(float input);

	void SetOperationWithInt(int32 opo);
	void SetOperation(Operation opo){ mOperation = opo; };
	void SetCrispInput(float input){ mCrispInput = input; };

	FuzzyVariable *GetFuzzyVariablePtr(){ return mVariablePtr; };
	Operation GetOperation(){ return mOperation; };
};
