// Fill out your copyright notice in the Description page of Project Settings.

#include "Z2.h"
#include "RuleDesc.h"

RuleDesc::RuleDesc(FuzzyVariable *varPtr, Operation opo)
{
	mVariablePtr = varPtr;
	mCrispInput = 0.0f;
}

RuleDesc::~RuleDesc()
{

}

void RuleDesc::CalculateDesirables(float input)
{
	mVariablePtr->CalculateDOM(input);
	mVariablePtr->CalculateDesirables();
}

void RuleDesc::SetOperationWithInt(int32 opo)
{
	if (opo == 0)
		mOperation = AND;
	else if (opo == 1)
		mOperation = OR;
	else
		mOperation = NOT;
}