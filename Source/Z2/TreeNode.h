// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
/**
 * 
 */
class Z2_API TreeNode
{
private:
	int32 Tier;
	bool Unlocked;
	FString AbilityName;

	TreeNode *NextTierNode;

	TreeNode *NegativeUpgradeNode;
	TreeNode *PositiveUpgradeNode;

	TreeNode *NegativeChildNode;

public:
	TreeNode(int32 tier, FString abilityName);
	~TreeNode();

	void SetNextTierNode(TreeNode *node){ NextTierNode = node; }
	void SetNegativeChildNode(TreeNode *node){ NegativeChildNode = node; }
	void SetNegativeUpgradeNode(TreeNode *node){ NegativeUpgradeNode = node; }
	void SetPositiveUpgradeNode(TreeNode *node){ PositiveUpgradeNode = node; }
	void SetUnlocked(bool unlocked){ Unlocked = unlocked; }

	int32 GetTier(){ return Tier; }
	FString GetAbilityName(){ return AbilityName; }
	TreeNode* GetNextTierNode(){ return NextTierNode; }
	TreeNode* GetNegativeChildNode(){ return NegativeChildNode; }
	TreeNode* GetNegativeUpgradeNode(){ return NegativeUpgradeNode; }
	TreeNode* GetPositiveUpgradeNode(){ return PositiveUpgradeNode; }
	bool IsUnlocked(){ return Unlocked; }
};
