// Fill out your copyright notice in the Description page of Project Settings.

#include "Z2.h"
#include "LogicManager.h"

LogicManager::LogicManager()
{
	DesInitialized = false;

	InitializeDesirabilityVariable();
}

LogicManager::~LogicManager()
{

}

void LogicManager::LoadFuzzySystem(FString xmlFileName)
{
	FXmlFile xmlFile(xmlFileName);

	//fuzzySystem wrapper
	FXmlNode *root = xmlFile.GetRootNode();

	////---------------------------------------------------------------------------------------------------------------------------------------
	////fuzzyVarVal wrapper
	////---------------------------------------------------------------------------------------------------------------------------------------
	const FXmlNode *varVal = root->GetFirstChildNode();

	////---------------------------------------------------------------------------------------------------------------------------------------
	////fuzzyVariable wrapper
	////---------------------------------------------------------------------------------------------------------------------------------------
	for (const FXmlNode *node = varVal->GetFirstChildNode(); node; node = node->GetNextNode())
	{
		TArray<FXmlAttribute> attArray1 = node->GetAttributes();

		FuzzyVariable* tempVar = CreateFuzzyVariable(attArray1[0].GetValue());

		for (const FXmlNode *node2 = node->GetFirstChildNode(); node2; node2 = node2->GetNextNode())
		{
			unsigned int i = 0;
			//Value
			FuzzyValue* tempVal = CreateFuzzyValue(tempVar, L"", DESIRABLE);

			TArray<FXmlAttribute> attArray1 = node2->GetAttributes();
			for (FXmlAttribute attIter : attArray1)
			{
				FString valName = attIter.GetValue();
				if (i == 0)
				{
					tempVal->SetValueName(valName);
					i++;
				}
				else if (i == 1)
				{
					int32 cons = FCString::Atoi(*valName);
					tempVal->SetConsequentWithInt(FCString::Atoi(*valName) - 1);
					i++;
				}
			}

				for (const FXmlNode *node3 = node2->GetFirstChildNode(); node3; node3 = node3->GetNextNode())
				{
					//Line
					FuzzyLine* tempLine = CreateLine(tempVal, 0.0f, 0.0f, 0.0f, 0.0f);

					unsigned int k = 0;

					TArray<FXmlAttribute> attArray2 = node3->GetAttributes();
					for (FXmlAttribute attIter : attArray2)
					{
						FString sNum = attIter.GetValue();
						float fNum = FCString::Atof(*sNum);

						//X1,Y1,X2,Y2
						if (k == 0)
						{
							tempLine->SetX1(fNum);
							k++;
						}
						else if (k == 1)
						{
							tempLine->SetY1(fNum);
							k++;
						}
						else if (k == 2)
						{
							tempLine->SetX2(fNum);
							k++;
						}
						else if (k > 2)
						{
							tempLine->SetY2(fNum);
							tempLine->CalculateSlope();
							k++;
						}
					}
				}
			}
		}
	
	//--------------------------------------------------------------------------------------------------------------------------------------
	//fuzzyRules wrapper
	//--------------------------------------------------------------------------------------------------------------------------------------
	const FXmlNode *fRules = varVal->GetNextNode();
	{
		for (const FXmlNode *node1 = fRules->GetFirstChildNode(); node1; node1 = node1->GetNextNode())
		{
			TArray<FXmlAttribute> attArray3 = node1->GetAttributes();

			FString ruleName = attArray3[0].GetValue();

			FuzzyRule* rule = CreateFuzzyRule(ruleName);

			for (const FXmlNode *node2 = node1->GetNextNode(); node2; node2 = node2->GetNextNode())
			{
				TArray<FXmlAttribute> attArray4 = node2->GetAttributes();

				FString nameStr = attArray4[0].GetValue();

				FuzzyVariable* temp = FindVariableByName(nameStr);

				int32 oNum = FCString::Atoi(*attArray4[1].GetValue());

				RuleDesc* param = CreateRuleDesc(rule, temp, NOT);
				param->SetOperationWithInt(oNum);
			}
		}
	}
}

FuzzyVariable* LogicManager::CreateFuzzyVariable(FString name)
{
	FuzzyVariable* temp = new FuzzyVariable(name);
	mVariableHash.Add(name, temp);

	return temp;
}

FuzzyValue* LogicManager::CreateFuzzyValue(FuzzyVariable* var, FString name, Consequent cons)
{
	FuzzyValue* temp = new FuzzyValue(name, cons);
	var->AddFuzzyValue(temp);

	return temp;
}

FuzzyLine* LogicManager::CreateLine(FuzzyValue* val, float x1, float y1, float x2, float y2)
{
	FuzzyLine* temp = new FuzzyLine(x1, y1, x2, y2);
	val->AddLine(temp);

	return temp;
}

FuzzyRule* LogicManager::CreateFuzzyRule(FString name)
{
	FuzzyRule* temp = new FuzzyRule(name);
	mRuleHash.Add(name, temp);

	return temp;
}

RuleDesc* LogicManager::CreateRuleDesc(FuzzyRule* rule, FuzzyVariable* var, Operation opo)
{
	RuleDesc* temp = new RuleDesc(var, opo);
	rule->AddParamToRule(temp);

	return temp;
}

FuzzyVariable* LogicManager::FindVariableByName(FString name)
{
	FuzzyVariable* variable = *mVariableHash.Find(name);

	if (!variable)
		return NULL;

	return variable;
}

FuzzyRule* LogicManager::FindRuleByName(FString name)
{
	FuzzyRule* rule = *mRuleHash.Find(name);

	return rule;
}

void LogicManager::DeleteVariableByName(FString name)
{
	FuzzyVariable* temp = *mVariableHash.Find(name);

	mVariableHash.Remove(temp->GetVariableName());
}

void LogicManager::DeletRuleByName(FString name)
{
	FuzzyRule* temp = *mRuleHash.Find(name);

	mRuleHash.Remove(temp->GetRuleName());
}

float LogicManager::CheckRule(FString ruleName, TArray<float> num, DefuzzProcess proc)
{
	float confidence = 0.0f;

	FuzzyRule* rule = FindRuleByName(ruleName);

	FuzzyVariable* desVal = FindVariableByName("Desirability");

	confidence = rule->CheckRule(num, proc, desVal);

	return confidence;
}

void LogicManager::InitializeDesirabilityVariable()
{
	//--------------------------------------------------------------------------------------------------------------------------------------
	//Desirability Variable
	//--------------------------------------------------------------------------------------------------------------------------------------

	if (!DesInitialized)
	{
		FuzzyVariable* desire = CreateFuzzyVariable(TEXT("Desirability"));
		{
			FuzzyValue* unDes = CreateFuzzyValue(desire, TEXT("UnDesirable"), CNONE);
			{
				CreateLine(unDes, 0.0f, 1.0f, 25.0f, 1.0f);
				CreateLine(unDes, 25.0f, 1.0f, 50.0f, 0.0f);
			}
			FuzzyValue* Des = CreateFuzzyValue(desire, TEXT("Desirable"), CNONE);
			{
				CreateLine(Des, 25.0f, 0.0f, 50.0f, 1.0f);
				CreateLine(Des, 50.0f, 1.0f, 75.0f, 0.0f);
			}
			FuzzyValue* veryDes = CreateFuzzyValue(desire, TEXT("VeryDesirable"), CNONE);
			{
				CreateLine(veryDes, 50.0f, 0.0f, 75.0f, 1.0f);
				CreateLine(veryDes, 75.0f, 1.0f, 100.0f, 1.0f);
			}
		}

		DesInitialized = true;
	}
}
