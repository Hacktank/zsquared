// Fill out your copyright notice in the Description page of Project Settings.

#include "Z2.h"
#include "CheckDesc.h"

CheckDesc::CheckDesc(FString ruleName, FString returnName, DefuzzProcess defuzz, int32 numVariables, ...)
{
	va_list fValues;

	va_start(fValues, numVariables);
	for (int32 i = 0; i < numVariables; i++)
		mCrispInputArr.Add(va_arg(numVariables, float));
	va_end(fValues);

	mRuleName = ruleName;
	mDefuzzification = defuzz;
	mReturnName = returnName;
}

CheckDesc::~CheckDesc()
{

}
