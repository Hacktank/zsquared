// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "BaseAbility.h"

/**
 * 
 */
class Z2_API KickboxAbility : public BaseAbility
{
public:
	KickboxAbility(FString name);
	~KickboxAbility();

	virtual void ExecuteAbility() override;
};
