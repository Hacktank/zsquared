// Fill out your copyright notice in the Description page of Project Settings.

#include "Z2.h"
#include "TreeNode.h"

TreeNode::TreeNode(int32 tier, FString abilityName)
{
	Tier = tier;
	Unlocked = false;
	AbilityName = abilityName;

	NegativeChildNode = nullptr;
	NegativeUpgradeNode = nullptr;
	PositiveUpgradeNode = nullptr;
}

TreeNode::~TreeNode()
{

}
