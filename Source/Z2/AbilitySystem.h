// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "AbilityTree.h"
#include "BaseAbility.h"

/**
 * 
 */
class Z2_API AbilitySystem
{
private:
	BaseAbility *StandardAbility;
	BaseAbility *AbilityOne;
	BaseAbility *AbilityTwo;
	BaseAbility *AbilityThree;

	AbilityTree *CharacterAbilityTree;
	TMap<FString, BaseAbility*> AbilityMap;

public:
	AbilitySystem(FString filePath);
	~AbilitySystem();

	void LoadAbilityMap();
	BaseAbility* GetAbilityByName(FString name);


	void SetStandardAbility(FString name){ StandardAbility = *AbilityMap.Find(name); }
	void SetAbilityOne(FString name){ AbilityOne = *AbilityMap.Find(name); }
	void SetAbilityTwo(FString name){ AbilityTwo = *AbilityMap.Find(name); }
	void SetAbilityThree(FString name){ AbilityThree = *AbilityMap.Find(name); }

	BaseAbility* GetStandardAbility(){ return StandardAbility; }
	BaseAbility* GetAbilityOne(){ return AbilityOne; }
	BaseAbility* GetAbilityTwo(){ return AbilityTwo; }
	BaseAbility* GetAbilityThree(){ return AbilityThree; }

	AbilityTree* GetAbilityTree(){ return CharacterAbilityTree; }
	TMap<FString, BaseAbility*> GetAbilityMap(){ return AbilityMap; }
};
