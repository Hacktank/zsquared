// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "BaseAbility.h"

/**
 * 
 */
class Z2_API TetherAbility : public BaseAbility
{
public:
	TetherAbility(FString name);
	~TetherAbility();

	virtual void ExecuteAbility() override;
};
