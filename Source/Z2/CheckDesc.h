// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
 * 
 */
enum DefuzzProcess{
	MEANOFMAXIMA,
	CENTROID
};

class Z2_API CheckDesc
{
private:
	FString mRuleName;
	FString mReturnName;
	TArray<float> mCrispInputArr;
	DefuzzProcess mDefuzzification;

public:
	CheckDesc(FString ruleName, FString returnName, DefuzzProcess defuzz, int32 numVariables, ...);
	~CheckDesc();

	FString GetRuleName(){ return mRuleName; };
	FString GetReturnName(){ return mReturnName; };
	DefuzzProcess GetDefuzzification(){ return mDefuzzification; };
	TArray<float> GetCrispInputArray(){ return mCrispInputArr; };
};
