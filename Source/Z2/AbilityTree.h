// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "TreeNode.h"
#include "Runtime/XmlParser/Public/XmlFile.h"

/**
 * 
 */
class Z2_API AbilityTree
{
private:
	TreeNode *RootNode;
	TArray<TreeNode*> NodeArray;

public:
	AbilityTree();
	~AbilityTree();

	void UnlockNextTier();

	void LoadTreeFromXML(FString filePath);
	TArray<TreeNode*> GetTierNodeArray(int32 tier);
};
