// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "BaseAbility.h"

/**
 * 
 */
class Z2_API EarthquakeAbility : public BaseAbility
{
public:
	EarthquakeAbility(FString name);
	~EarthquakeAbility();

	virtual void ExecuteAbility() override;
};
