// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
 * 
 */
class Z2_API FuzzyLine
{
private:
	FVector2D mPoint1, mPoint2;
	float mSlope;
	float mDOM;
	float mValue;

public:
	FuzzyLine(float x1, float y1, float x2, float y2);
	~FuzzyLine();

	void CalculateSlope();
	float CalculateDOM(float input);
	float CalculateValueWithDOM(float value);

	void SetPoints(float x1, float y1, float x2, float y2);
	void SetX1(float num){ mPoint1.X = num; };
	void SetY1(float num){ mPoint1.Y = num; };
	void SetX2(float num){ mPoint2.X = num; };
	void SetY2(float num){ mPoint2.Y = num; };

	float GetDOM(){ return mDOM; };
	FVector2D GetPoint1(){ return mPoint1; };
	FVector2D GetPoint2(){ return mPoint2; };
	float GetValue(){ return mValue; };
};
