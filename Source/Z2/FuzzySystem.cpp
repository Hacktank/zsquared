// Fill out your copyright notice in the Description page of Project Settings.

#include "Z2.h"
#include "FuzzySystem.h"

UFuzzySystem::UFuzzySystem()
{
	mManager = new LogicManager();
}

UFuzzySystem::~UFuzzySystem()
{

}

void UFuzzySystem::LoadSystemFromXML(FString xmlFileName)
{
	mManager->LoadFuzzySystem(xmlFileName);
}

void UFuzzySystem::AddVariable(FString varName)
{
	mManager->CreateFuzzyVariable(varName);

	//FXML->CreateNewVariable(id, varName);
}

bool UFuzzySystem::AddValueToVariable(FString varName, FString valName, Consequent cons)
{
	FuzzyVariable* temp = mManager->FindVariableByName(varName);
	//XMLVariable* xtemp = FXML->FindXMLVariableById(temp->GetId());

	if (temp)
	{
		mManager->CreateFuzzyValue(temp, valName, cons);

		//FXML->CreateNewValue(id, xtemp, valName, cons);

		return true;
	}

	return false;
}

bool UFuzzySystem::AddLineToValue(FString varName, FString valName, float x1, float y1, float x2, float y2)
{
	FuzzyVariable* tempVar = NULL;
	FuzzyValue* tempVal = NULL;
	//XMLVariable* xtempVar = NULL;
	//XMLValue* xtempVal = NULL;

	tempVar = mManager->FindVariableByName(varName);

	if (tempVar)
	{
		tempVal = tempVar->FindValueByName(valName);
		//xtempVar = FXML->FindXMLVariableById(tempVar->GetId());
	}
	else
		return false;

	if (tempVal)
	{
		mManager->CreateLine(tempVal, x1, y1, x2, y2);

		//xtempVal = xtempVar->FindXMLValueById(tempVal->GetId());

		//FXML->CreateNewLine(xtempVal->GetNextValidLineId(), xtempVal, x1, y1, x2, y2);

		return true;
	}

	return false;
}

void UFuzzySystem::AddRule(FString ruleName)
{

	mManager->CreateFuzzyRule(ruleName);

	//FXML->CreateNewRule(id, ruleName);
}

bool UFuzzySystem::AddDescToRule(FString ruleName, FString varName, Operation opo)
{
	FuzzyRule* tempRul = NULL;
	FuzzyVariable* tempVar = NULL;
	//XMLRule* xtempRule = NULL;
	//XMLVariable* xtempVar = NULL;

	tempRul = mManager->FindRuleByName(ruleName);

	if (tempRul)
	{
		tempVar = mManager->FindVariableByName(varName);
		//xtempVar = FXML->FindXMLVariableById(tempVar->GetId());
	}
	else
		return false;

	if (tempVar)
	{
		mManager->CreateRuleDesc(tempRul, tempVar, opo);

		//xtempRule = FXML->FindXMLRuleById(tempRul->GetId());

		//FXML->CreateNewRuleParam(tempRul->GetNextValidParamId(), xtempRule, opo, xtempVar);

		return true;
	}

	return false;
}

bool UFuzzySystem::DeleteVariableByName(FString name)
{
	FuzzyVariable* tempVar = mManager->FindVariableByName(name);

	if (tempVar)
	{
		mManager->DeleteVariableByName(name);
		//FXML->DeleteVariableById(tempVar->GetId());

		return true;
	}

	return false;
}

bool UFuzzySystem::DeleteRuleByName(FString name)
{
	FuzzyRule* tempRule = mManager->FindRuleByName(name);

	if (tempRule)
	{
		mManager->DeletRuleByName(name);
		//FXML->DeleteRuleById(tempRule->GetId());

		return true;
	}

	return false;
}

float UFuzzySystem::CheckRule(FString ruleName, DefuzzProcess proc, TArray<float> numVec)
{
	float confidence = 0.0f;

	confidence = mManager->CheckRule(ruleName, numVec, proc);

	ClearVariableDesirabilitiesByName(ruleName);

	return confidence;
}

void UFuzzySystem::InitializeDesirabilityVariable()
{
	mManager->InitializeDesirabilityVariable();
}

FString UFuzzySystem::FindGreatestConfidence()
{
	float greatestConf = 0.0f;
	float temp = 0.0f;
	CheckDesc* desc = NULL;

	for (auto checkIter = mManager->GetCheckDescHash().CreateIterator(); checkIter; ++checkIter)
	{
		temp = CheckRule(checkIter->Value->GetRuleName(), checkIter->Value->GetDefuzzification(), checkIter->Value->GetCrispInputArray());

		if (temp > greatestConf)
		{
			greatestConf = temp;
			desc = checkIter->Value;
		}

		ClearVariableDesirabilitiesByName(checkIter->Value->GetRuleName());
	}

	return desc->GetReturnName();
}

void UFuzzySystem::ClearAllVariableDesirabilities()
{
	for (auto varIter = mManager->GetVariableHash().CreateIterator(); varIter; ++varIter)
		varIter->Value->ClearDesirables();
}

void UFuzzySystem::ClearVariableDesirabilitiesByName(FString ruleName)
{
	FuzzyRule* temp = mManager->FindRuleByName(ruleName);

	for (int32 i = 0; i < temp->GetRuleDescArr().Num(); i++)
		temp->GetRuleDescArr()[i]->GetFuzzyVariablePtr()->ClearDesirables();
}


