// Fill out your copyright notice in the Description page of Project Settings.

#include "Z2.h"
#include "AbilityTree.h"

AbilityTree::AbilityTree()
{
	RootNode = nullptr;
}

AbilityTree::~AbilityTree()
{

}

void AbilityTree::UnlockNextTier()
{
	bool working = true;
	TreeNode* node = RootNode;

	while (working)
	{
		if (!node->GetNegativeChildNode()->IsUnlocked())
			working = false;
		else
			node = node->GetNegativeChildNode();
	}

	while (node)
	{
		node->SetUnlocked(true);

		node = node->GetNextTierNode();
	}
}

TArray<TreeNode*> AbilityTree::GetTierNodeArray(int32 tier)
{
	TArray<TreeNode*> tempArr;

	for (int32 i = 0; i < NodeArray.Num(); i++)
	{
		if (NodeArray[i]->GetTier() == tier)
			tempArr.Push(NodeArray[i]);
	}

	return tempArr;
}

void AbilityTree::LoadTreeFromXML(FString filePath)
{
	//Open XML file
	FXmlFile xmlFile(filePath);

	//Get root node
	FXmlNode *root = xmlFile.GetRootNode();
	
	//Get standard ability
	const FXmlNode *ability = root->GetFirstChildNode();
	TArray<FXmlAttribute> attArray = ability->GetAttributes();
	RootNode = new TreeNode(FCString::Atoi(*attArray[1].GetValue()), attArray[0].GetValue());
	NodeArray.Push(RootNode);

	TreeNode *currentRootNode = RootNode;
	TreeNode *currentNode = RootNode;

	for (const FXmlNode *node = ability->GetNextNode(); node; node = node->GetNextNode())
	{
		attArray.Empty();
		attArray = node->GetAttributes();

		int32 tier = FCString::Atoi(*attArray[1].GetValue());
		TreeNode *nextNode = new TreeNode(tier, attArray[0].GetValue());
		NodeArray.Push(nextNode);

		if (currentNode->GetTier() == tier)
			currentNode->SetNextTierNode(nextNode);
		else
		{
			currentRootNode->SetNegativeChildNode(nextNode);
			currentRootNode = nextNode;
		}

		currentNode = nextNode;
	}
}