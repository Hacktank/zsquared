// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "FuzzyValue.h"
/**
 * 
 */
class Z2_API FuzzyVariable
{
private:
	FString mVariableName;
	float mUnDesireableValue;
	float mDesireableValue;
	float mVeryDesireableValue;
	TMap<FString, FuzzyValue*> mValueHash;

public:
	FuzzyVariable(FString variableName);
	~FuzzyVariable();

	void AddFuzzyValue(FuzzyValue* val);
	void CalculateDOM(float num);
	void CalculateValueFromDOM(float num);
	//void CalculateValueFromDOM(float num, FString valName);
	int32 GetNextValidValueId();
	FuzzyValue* FindValueByName(FString name);
	void CalculateDesirables();
	void ClearDesirables();

	FString GetVariableName(){ return mVariableName; };
	TMap<FString, FuzzyValue*> GetValuesHash(){ return mValueHash; };
	FuzzyValue* GetValueByName(FString name);
	float GetUnDesireableValue(){ return mUnDesireableValue; };
	float GetDesireableValue(){ return mDesireableValue; };
	float GetVeryDesireableValue(){ return mVeryDesireableValue; };

	void SetUnDesireableValue(float num){ mUnDesireableValue = num; };
	void SetDesireableValue(float num){ mDesireableValue = num; };
	void SetVeryDesireableValue(float num){ mVeryDesireableValue = num; };
};
