// Fill out your copyright notice in the Description page of Project Settings.

#include "Z2.h"
#include "AbilitySystem.h"
#include "MountedGun.h"
#include "EarthQuakeAbility.h"
#include "BullRushAbility.h"
#include "KickboxAbility.h"
#include "BandSlingshotAbility.h"
#include "MediaBlitzAbility.h"
#include "LinuxBlasterAbility.h"
#include "TetherAbility.h"

AbilitySystem::AbilitySystem(FString filePath)
{
	CharacterAbilityTree = new AbilityTree();
	CharacterAbilityTree->LoadTreeFromXML(filePath);

	LoadAbilityMap();
}

AbilitySystem::~AbilitySystem()
{

}

BaseAbility* AbilitySystem::GetAbilityByName(FString name)
{
	return *AbilityMap.Find(name);
}

void AbilitySystem::LoadAbilityMap()
{
	//Mounted Gun Ability
	MountedGunAbility *mountedGun = new MountedGunAbility(TEXT("MountedGun"));
	AbilityMap.Emplace(mountedGun->GetAbilityName(), mountedGun);

	//Earthquake Ability
	EarthquakeAbility *earthquake = new EarthquakeAbility(TEXT("Earthquake"));
	AbilityMap.Emplace(earthquake->GetAbilityName(), earthquake);

	//Bull Rush Ability
	BullRushAbility *bullRush = new BullRushAbility(TEXT("BullRush"));
	AbilityMap.Emplace(bullRush->GetAbilityName(), bullRush);

	//Kickbox Ability
	KickboxAbility *kickbox = new KickboxAbility(TEXT("Kickbox"));
	AbilityMap.Emplace(kickbox->GetAbilityName(), kickbox);

	//Band Sling Shot
	BandSlingshotAbility *bandSlingShot = new BandSlingshotAbility(TEXT("BandSlingShot"));
	AbilityMap.Emplace(bandSlingShot->GetAbilityName(), bandSlingShot);

	//Media Blitz
	MediaBlitzAbility *mediaBlitz = new MediaBlitzAbility(TEXT("MediaBlitz"));
	AbilityMap.Emplace(mediaBlitz->GetAbilityName(), mediaBlitz);

	//Linux Blaster
	LinuxBlasterAbility *linuxBlaster = new LinuxBlasterAbility(TEXT("LinuxBlaster"));
	AbilityMap.Emplace(linuxBlaster->GetAbilityName(), linuxBlaster);

	//Tether
	TetherAbility *tether = new TetherAbility(TEXT("Tether"));
	AbilityMap.Emplace(tether->GetAbilityName(), tether);
}
