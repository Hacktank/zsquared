// Fill out your copyright notice in the Description page of Project Settings.

#include "Z2.h"
#include "FuzzyValue.h"

FuzzyValue::FuzzyValue(FString valueName, Consequent fCons)
{
	mValueName = valueName;
	mFuzzyConsequent = fCons;
	mValue = 0.0f;
}

FuzzyValue::~FuzzyValue()
{

}

void FuzzyValue::AddLine(FuzzyLine *line)
{
	mLineArr.Add(line);
}

int32 FuzzyValue::GetNextValidLineId()
{
	return mLineArr.Num();
}

float FuzzyValue::CalculateDOM(float input)
{
	float temp = 0.0f;
	float dom = 0.0f;

	for (int32 i = 0; i < mLineArr.Num(); i++)
	{
		dom = mLineArr[i]->CalculateDOM(input);

		if (dom > temp)
			temp = dom;
	}

	return temp;
}

void FuzzyValue::SetConsequentWithInt(int32 num)
{
	if (num < 1 || num > 2)
		mFuzzyConsequent = UNDESIRABLE;
	else if (num == 1)
		mFuzzyConsequent = DESIRABLE;
	else
		mFuzzyConsequent = VERYDESIRABLE;
}
