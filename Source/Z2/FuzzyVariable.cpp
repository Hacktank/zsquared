// Fill out your copyright notice in the Description page of Project Settings.

#include "Z2.h"
#include "FuzzyVariable.h"

FuzzyVariable::FuzzyVariable(FString variableName)
{
	mVariableName = variableName;

	mUnDesireableValue = 0.0f;
	mDesireableValue = 0.0f;
	mVeryDesireableValue = 0.0f;
}

FuzzyVariable::~FuzzyVariable()
{

}

void FuzzyVariable::AddFuzzyValue(FuzzyValue* val)
{
	mValueHash.Add(val->GetValueName(), val);
}

void FuzzyVariable::CalculateDOM(float num)
{
	float dom = 0.0f;
	float temp = 0.0f;

	for (auto valueIter = mValueHash.CreateIterator(); valueIter; ++valueIter)
	{
		dom = 0.0f;

		for (FuzzyLine* lineIter : valueIter->Value->GetLineArr())
		{
			temp = lineIter->CalculateDOM(num);

			if (temp > dom)
				dom = temp;
		}

		valueIter->Value->SetValue(dom);
	}
}

void FuzzyVariable::CalculateValueFromDOM(float num)
{
	for (auto valueIter = mValueHash.CreateIterator(); valueIter; ++valueIter)
	{
		for (FuzzyLine* lineIter : valueIter->Value->GetLineArr())
		{
			float val = lineIter->CalculateValueWithDOM(num);

			valueIter->Value->SetValue(val);
		}
	}
}

//void FuzzyVariable::CalculateValueFromDOM(float num, FString valName)
//{
//
//}

int32 FuzzyVariable::GetNextValidValueId()
{
	return mValueHash.Num();
}

FuzzyValue* FuzzyVariable::FindValueByName(FString name)
{
	FuzzyValue* value = *mValueHash.Find(name);

	if (!value)
		return NULL;

	return value;
}

void FuzzyVariable::CalculateDesirables()
{
	float uDes = 0.0f;
	float Des = 0.0f;
	float vDes = 0.0f;

	for (auto valueIter = mValueHash.CreateIterator(); valueIter; ++valueIter)
	{
		if (valueIter->Value->GetFuzzyConsequent() == UNDESIRABLE && valueIter->Value->GetValue() > uDes)
		{
			uDes = valueIter->Value->GetValue();
			mUnDesireableValue = uDes;
		}
		else if (valueIter->Value->GetFuzzyConsequent() == DESIRABLE && valueIter->Value->GetValue() > Des)
		{
			Des = valueIter->Value->GetValue();
			mDesireableValue = Des;
		}
		else if (valueIter->Value->GetFuzzyConsequent() == VERYDESIRABLE && valueIter->Value->GetValue() > vDes)
		{
			vDes = valueIter->Value->GetValue();
			mVeryDesireableValue = vDes;
		}
	}
}

void FuzzyVariable::ClearDesirables()
{
	mUnDesireableValue = 0.0f;
	mDesireableValue = 0.0f;
	mVeryDesireableValue = 0.0f;
}
