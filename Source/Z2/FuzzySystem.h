// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "LogicManager.h"
#include "Object.h"
#include "FuzzySystem.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class Z2_API UFuzzySystem : public UObject
{
	GENERATED_BODY()
	
private:
	LogicManager *mManager;

public:
	UFuzzySystem();
	~UFuzzySystem();

	void LoadSystemFromXML(FString xmlFileName);

	void AddCheckDesc(CheckDesc* decl){ mManager->AddCheckDesc(decl); };
	void AddVariable(FString varName);
	bool AddValueToVariable(FString varName, FString valName, Consequent cons);
	bool AddLineToValue(FString varName, FString valName, float x1, float y1, float x2, float y2);
	void AddRule(FString ruleName);
	bool AddDescToRule(FString ruleName, FString varName, Operation opo);

	bool DeleteVariableByName(FString name);
	bool DeleteRuleByName(FString name);

	float CheckRule(FString ruleName, DefuzzProcess proc, TArray<float> numVec);

	void InitializeDesirabilityVariable();

	UFUNCTION(BlueprintCallable, Category = FuzzySystem)
	FString FindGreatestConfidence();

	void ClearCheckVec(){ mManager->ClearCheckVec(); };
	void ClearAllVariableDesirabilities();
	void ClearVariableDesirabilitiesByName(FString ruleName);
};
