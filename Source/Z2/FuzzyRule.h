// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "RuleDesc.h"
#include "CheckDesc.h"
/**
 * 
 */
class Z2_API FuzzyRule
{
private:
	FString mRuleName;
	TArray<RuleDesc*> mRuleDescArr;

public:
	FuzzyRule(FString ruleName);
	~FuzzyRule();

	float Defuzzification(DefuzzProcess proc, FuzzyVariable* desVar, FVector des);
	float And(float a, float b);
	float Or(float a, float b);
	float CheckRule(TArray<float> numsVec, DefuzzProcess proc, FuzzyVariable* desVar);
	FVector CalculateOperation();
	float MeanOfMaxima(FuzzyVariable* desVar, FVector desirability);
	float Centroide(FuzzyVariable* desVar, FVector desirability);

	void AddParamToRule(RuleDesc* param){ mRuleDescArr.Add(param); };
	TArray<RuleDesc*> GetRuleDescArr(){ return mRuleDescArr; };
	FString GetRuleName(){ return mRuleName; };
};
