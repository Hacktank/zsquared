// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
 * 
 */
class Z2_API BaseAbility
{
private:
	FString AbilityName;

public:
	BaseAbility(FString name);
	~BaseAbility();

	virtual void ExecuteAbility();

	FString GetAbilityName(){ return AbilityName; }
};
