// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "BaseAbility.h"

/**
 * 
 */
class Z2_API MediaBlitzAbility : public BaseAbility
{
public:
	MediaBlitzAbility(FString name);
	~MediaBlitzAbility();

	virtual void ExecuteAbility() override;
};
