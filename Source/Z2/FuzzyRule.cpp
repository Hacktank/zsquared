// Fill out your copyright notice in the Description page of Project Settings.

#include "Z2.h"
#include "FuzzyRule.h"

FuzzyRule::FuzzyRule(FString ruleName)
{
	mRuleName = ruleName;
}

FuzzyRule::~FuzzyRule()
{

}

float FuzzyRule::Defuzzification(DefuzzProcess proc, FuzzyVariable* desVar, FVector des)
{
	float confidence = 0.0f;

	if (proc == MEANOFMAXIMA)
	{
		confidence = MeanOfMaxima(desVar, des);
	}
	else if (proc == CENTROID)
	{
		confidence = Centroide(desVar, des);
	}

	return confidence;
}

float FuzzyRule::And(float a, float b)
{
	if (a < b)
		return a;

	return b;
}

float FuzzyRule::Or(float a, float b)
{
	if (a > b)
		return a;

	return b;
}

float FuzzyRule::CheckRule(TArray<float> numsArr, DefuzzProcess proc, FuzzyVariable* desVar)
{
	for (int32 i = 0; i < mRuleDescArr.Num(); i++)
		mRuleDescArr[i]->CalculateDesirables(numsArr[i]);

	FVector desirable = CalculateOperation();

	float confidence = Defuzzification(proc, desVar, desirable);

	return confidence;
}

FVector FuzzyRule::CalculateOperation()
{
	bool calculating = true;
	int iter = 0;

	float udes2 = 0.0f;
	float des2 = 0.0f;
	float vdes2 = 0.0f;

	FVector desirable;

	float udes1 = mRuleDescArr[iter]->GetFuzzyVariablePtr()->GetUnDesireableValue();
	float des1 = mRuleDescArr[iter]->GetFuzzyVariablePtr()->GetDesireableValue();
	float vdes1 = mRuleDescArr[iter]->GetFuzzyVariablePtr()->GetVeryDesireableValue();
	Operation op = mRuleDescArr[iter]->GetOperation();

	while (calculating)
	{
		if (op == NOT)
			calculating = false;
		else if (op == AND)
		{
			iter++;

			udes2 = mRuleDescArr[iter]->GetFuzzyVariablePtr()->GetUnDesireableValue();
			desirable.X = And(udes1, udes2);
			des2 = mRuleDescArr[iter]->GetFuzzyVariablePtr()->GetDesireableValue();
			desirable.Y = And(des1, des2);
			vdes2 = mRuleDescArr[iter]->GetFuzzyVariablePtr()->GetVeryDesireableValue();
			desirable.Z = And(vdes1, vdes2);
		}
		else if (op == OR)
		{
			iter++;

			udes2 = mRuleDescArr[iter]->GetFuzzyVariablePtr()->GetUnDesireableValue();
			desirable.X = Or(udes1, udes2);
			des2 = mRuleDescArr[iter]->GetFuzzyVariablePtr()->GetDesireableValue();
			desirable.Y = Or(des1, des2);
			vdes2 = mRuleDescArr[iter]->GetFuzzyVariablePtr()->GetVeryDesireableValue();
			desirable.Z = Or(vdes1, vdes2);
		}

		op = mRuleDescArr[iter]->GetOperation();
		udes1 = desirable.X;
		des1 = desirable.Y;
		vdes1 = desirable.Z;
	}

	return desirable;
}

float FuzzyRule::MeanOfMaxima(FuzzyVariable* desVar, FVector desirability)
{
	float udes = 0.0f;
	float des = 0.0f;
	float vdes = 0.0f;
	float tempUdes = 0.0f;
	float tempDes = 0.0f;
	float tempVdes = 0.0f;
	int32 index = 0;

	FuzzyValue* UnDesirable = nullptr;
	FuzzyValue* Desirable = nullptr;
	FuzzyValue* VeryDesirable = nullptr;

	for (auto valueIter = desVar->GetValuesHash().CreateIterator(); valueIter; ++valueIter)
	{
		for (FuzzyLine* lineIter : valueIter->Value->GetLineArr())
		{
			if (index == 0)
			{
				UnDesirable = valueIter->Value;
				tempUdes = 0.0f;

				if (desirability.X > 0.0f)
					tempUdes = lineIter->CalculateValueWithDOM(desirability.X);

				if (tempUdes > udes)
					udes = tempUdes;
			}
			else if (index == 1)
			{
				Desirable = valueIter->Value;
				tempDes = 0.0f;

				if (desirability.Y > 0.0f)
					tempDes = lineIter->CalculateValueWithDOM(desirability.Y);

				if (tempDes > des)
					des = tempDes;
			}
			else if (index == 2)
			{
				VeryDesirable = valueIter->Value;
				tempVdes = 0.0f;

				if (desirability.Z > 0.0f)
					tempVdes = lineIter->CalculateValueWithDOM(desirability.Z);

				if (tempVdes > vdes)
					vdes = tempVdes;
			}
		}
		index++;
	}

	float total = 0.0f, p1 = 0.0f, p2 = 0.0f, p3 = 0.0f;
	index = 0;

	if (udes > 0.0f)
	{
		index++;

		p1 = udes / 2;
		total += p1;
	}
	if (des > 0.0f)
	{
		index++;

		float desV1 = 0.0f, desV2 = 0.0f;

		for (FuzzyLine* lineIter : Desirable->GetLineArr())
		{
			if (index == 0)
				desV1 = lineIter->GetValue();
			else
				desV2 = lineIter->GetValue();
		}

		p2 = (desV1 + desV2) / 2;
		total += p2;
	}
	if (vdes > 0.0f)
	{
		index++;

		p3 = (vdes + 100) / 2;
		total += p3;
	}

	if (index > 0)
		return total / index;

	return 0.0f;
}

float FuzzyRule::Centroide(FuzzyVariable* desVar, FVector desirability)
{
	return 0.0f;
}
