// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "FuzzyRule.h"
#include "Runtime/XmlParser/Public/XmlFile.h"

/**
 * 
 */
class Z2_API LogicManager
{
private:
	TMap<FString, FuzzyRule*> mRuleHash;
	TMap<FString, FuzzyVariable*> mVariableHash;
	TMap<FString, CheckDesc*> mCheckDescHash;

	bool DesInitialized;

public:
	LogicManager();
	~LogicManager();

	void LoadFuzzySystem(FString xmlFileName);

	FuzzyVariable* CreateFuzzyVariable(FString name);
	FuzzyValue* CreateFuzzyValue(FuzzyVariable* var, FString name, Consequent cons);
	FuzzyLine* CreateLine(FuzzyValue* val, float x1, float y1, float x2, float y2);
	FuzzyRule* CreateFuzzyRule(FString name);
	RuleDesc* CreateRuleDesc(FuzzyRule* rule, FuzzyVariable* var, Operation opo);
	FuzzyVariable* FindVariableByName(FString name);
	FuzzyRule* FindRuleByName(FString name);
	void DeleteVariableByName(FString name);
	void DeletRuleByName(FString name);
	float CheckRule(FString ruleName, TArray<float> num, DefuzzProcess proc);
	void InitializeDesirabilityVariable();
	void AddCheckDesc(CheckDesc* desc){ mCheckDescHash.Add(desc->GetReturnName(), desc); };
	void ClearCheckVec(){ mCheckDescHash.Empty(); };

	TMap<FString, FuzzyVariable*> GetVariableHash(){ return mVariableHash; };
	TMap<FString, FuzzyRule*> GetRuleHash(){ return mRuleHash; };
	TMap<FString, CheckDesc*> GetCheckDescHash(){ return mCheckDescHash; };
};
