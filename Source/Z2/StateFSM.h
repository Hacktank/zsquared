// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "AIState.h"

/**
 * 
 */
class Z2_API StateFSM
{
private:
	AIState *CurrentState;

public:
	StateFSM();
	~StateFSM();

	void ExecuteCurrentState();
	bool TransitionToState(AIState *newState);
};