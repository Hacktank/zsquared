// Fill out your copyright notice in the Description page of Project Settings.

#include "Z2.h"
#include "StateFSM.h"

StateFSM::StateFSM()
{

}

StateFSM::~StateFSM()
{

}

void StateFSM::ExecuteCurrentState()
{
	if (CurrentState)
		CurrentState->ExecuteState();
}

bool StateFSM::TransitionToState(AIState *newState)
{
	if (CurrentState)
		CurrentState->ExitState();

	CurrentState = newState;
	CurrentState->EnterState();
}