// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "BaseAbility.h"

/**
 * 
 */
class Z2_API BullRushAbility : public BaseAbility
{
public:
	BullRushAbility(FString name);
	~BullRushAbility();

	virtual void ExecuteAbility() override;
};
