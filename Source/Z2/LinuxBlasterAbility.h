// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "BaseAbility.h"

/**
 * 
 */
class Z2_API LinuxBlasterAbility : public BaseAbility
{
public:
	LinuxBlasterAbility(FString name);
	~LinuxBlasterAbility();

	virtual void ExecuteAbility() override;
};
