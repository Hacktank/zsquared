// Fill out your copyright notice in the Description page of Project Settings.

#include "Z2.h"
#include "FuzzyLine.h"

FuzzyLine::FuzzyLine(float x1, float y1, float x2, float y2)
{
	mPoint1.X = x1;
	mPoint1.Y = y1;
	mPoint2.X = x2;
	mPoint2.Y = y2;

	mDOM = 0.0f;
	mValue = 0.0f;

	CalculateSlope();
}

FuzzyLine::~FuzzyLine()
{

}

void FuzzyLine::CalculateSlope()
{
	mSlope = (mPoint2.Y - mPoint1.Y) / (mPoint2.X - mPoint1.X);
}

float FuzzyLine::CalculateDOM(float input)
{
	if (input < mPoint1.X || input > mPoint2.X)
	{
		mDOM = 0.0f;
		return mDOM;
	}

	return mDOM = (mSlope * input) - (mSlope * mPoint1.X) + mPoint1.Y;
}

float FuzzyLine::CalculateValueWithDOM(float value)
{
	if (value > 1.0f || value < 0.0f)
	{
		mValue = 0.0f;
		return mValue;
	}

	return mValue = ((value - mPoint1.Y) / mSlope) + mPoint1.X;
}
