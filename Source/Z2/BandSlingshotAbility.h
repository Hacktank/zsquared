// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "BaseAbility.h"

/**
 * 
 */
class Z2_API BandSlingshotAbility : public BaseAbility
{
public:
	BandSlingshotAbility(FString name);
	~BandSlingshotAbility();

	virtual void ExecuteAbility() override;
};
