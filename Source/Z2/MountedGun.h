// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "BaseAbility.h"

/**
 * 
 */
class Z2_API MountedGunAbility : public BaseAbility
{
public:
	MountedGunAbility(FString name);
	~MountedGunAbility();

	virtual void ExecuteAbility() override;
};
