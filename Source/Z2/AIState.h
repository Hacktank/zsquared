// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
 * 
 */
class Z2_API AIState
{
public:
	AIState();
	~AIState();

	virtual void EnterState();
	virtual void ExecuteState();
	virtual void ExitState();
};