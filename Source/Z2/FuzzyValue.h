// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "FuzzyLine.h"
/**
 * 
 */
enum Consequent{
	CNONE,
	UNDESIRABLE,
	DESIRABLE,
	VERYDESIRABLE
};

class Z2_API FuzzyValue
{
private:
	Consequent mFuzzyConsequent;
	TArray<FuzzyLine*> mLineArr;
	FString mValueName;
	float mValue;

public:
	FuzzyValue(FString valueName, Consequent fCons);
	~FuzzyValue();

	void AddLine(FuzzyLine *line);
	int32 GetNextValidLineId();
	float CalculateDOM(float input);
	void SetConsequentWithInt(int32 num);

	TArray<FuzzyLine*> GetLineArr(){ return mLineArr; };
	FString GetValueName(){ return mValueName; };
	float GetValue(){ return mValue; };
	Consequent GetFuzzyConsequent(){ return mFuzzyConsequent; };

	void SetValueName(FString name){ mValueName = name; };
	void SetConsequent(Consequent cons){ mFuzzyConsequent = cons; };
	void SetValue(float num){ mValue = num; };
};
